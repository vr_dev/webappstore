package com.example.webappstore.filter;

import com.example.webappstore.entity.User;
import com.example.webappstore.util.constants.CommandStorage;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.example.webappstore.util.constants.ConstantNames.USER;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;

public class UserUrlPermitFilter implements Filter {
    private static final Set<String> adminPages = new HashSet<>();
    private static final Set<String> userPages = new HashSet<>();
    private static final Set<String> guestPages = new HashSet<>();

    private void fillPermittedPages() {
        guestPages.add(CommandStorage.ROOT);
        guestPages.add(CommandStorage.ROOT_2);
        guestPages.add(CommandStorage.HOME);
        guestPages.add(CommandStorage.PRODUCT_CATALOG);
        guestPages.add(CommandStorage.LOGIN);
        guestPages.add(CommandStorage.PRODUCT);
        guestPages.add(CommandStorage.ADD_PRODUCT_TO_CART);
        guestPages.add(CommandStorage.CART);
        guestPages.add(CommandStorage.DELETE_PRODUCT_FROM_CART);
        guestPages.add(CommandStorage.LOGOUT);
        guestPages.add(CommandStorage.CHANGE_LANGUAGE);
        guestPages.add(CommandStorage.REGISTRATION);
        guestPages.add(CommandStorage.REGISTRATION_ADMIN);

        userPages.addAll(guestPages);
        userPages.add(CommandStorage.CREATE_ORDER);
        userPages.add(CommandStorage.ORDERS_USER);
        userPages.add(CommandStorage.ORDER_DETAIL);
        userPages.add(CommandStorage.ORDER);
        userPages.add(CommandStorage.CHANGE_STATUS);

        adminPages.addAll(userPages);
        adminPages.add(CommandStorage.CREATE_PRODUCT);
        adminPages.add(CommandStorage.UPDATE_PRODUCT);
        adminPages.add(CommandStorage.DELETE_PRODUCT);
        adminPages.add(CommandStorage.ALL_USERS);
        adminPages.add(CommandStorage.ACTIVATE_USER);
        adminPages.add(CommandStorage.DEACTIVATE_USER);
        adminPages.add(CommandStorage.ORDERS_ADMIN);
        adminPages.add(CommandStorage.DELETE_ORDER_ADMIN_SERVICE);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        fillPermittedPages();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        User currentUser = (User) servletRequest.getSession().getAttribute(USER);
        Set<String> permittedPages;

        if(currentUser==null) {
            permittedPages = guestPages;
        } else if (currentUser.isAdmin()) {
            permittedPages = adminPages;
        } else  {
            permittedPages = userPages;
        }


        String requestPage = servletRequest.getRequestURI().replace(servletRequest.getContextPath(), "");

        if (permittedPages.contains(requestPage)) {
            chain.doFilter(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse) response).sendRedirect(HOME_SERVICE);
        }
    }
}
