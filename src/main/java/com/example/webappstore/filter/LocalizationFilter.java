package com.example.webappstore.filter;

import com.example.webappstore.util.constants.ConstantNames;
import jakarta.servlet.*;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Locale;

/**
 * Servlet Filter implementation class LocalizationFilter
 *
 * @author Viktor Rogachov
 */
public class LocalizationFilter implements Filter {

    private static final String DEFAULT_CHARSET_ENCODING = "UTF-8";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        setLocalization(request, response);
        request.setCharacterEncoding(DEFAULT_CHARSET_ENCODING);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void setLocalization(HttpServletRequest request, HttpServletResponse response) {
        Object langAttribute = request.getSession().getAttribute(ConstantNames.LANGUAGE);
        if (langAttribute == null) {
            Cookie[] cookies = request.getCookies();
            if (cookies == null) {
                setLocaleToCookieAndSession(request, response);
            } else {
                String cookieLang = null;
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(ConstantNames.LANGUAGE)) {
                        cookieLang = cookie.getValue();
                        request.getSession().setAttribute(ConstantNames.LANGUAGE, cookieLang);
                    }
                }
                if (cookieLang == null) {
                    setLocaleToCookieAndSession(request, response);
                }
            }
        }
    }

    private void setLocaleToCookieAndSession(HttpServletRequest request, HttpServletResponse response) {
        String currentLang = request.getLocale().getLanguage();
        Locale resultLocale = LocaleStorage.getLocaleFromLanguage(currentLang).getLocale();
        Cookie langCookie = new Cookie(ConstantNames.LANGUAGE, resultLocale.getLanguage());
        response.addCookie(langCookie);
        request.getSession().setAttribute(ConstantNames.LANGUAGE, currentLang);
    }

    enum LocaleStorage {

        ENG(Locale.US),
        RUS(new Locale("ru", "RU"));

        private final Locale locale;

        LocaleStorage(Locale locale) {
            this.locale = locale;
        }

        public static LocaleStorage getLocaleFromLanguage(String inputLanguage) {
            for (LocaleStorage currentLocale : LocaleStorage.values()) {
                if (currentLocale.locale.getLanguage().equals(inputLanguage)) {
                    return currentLocale;
                }
            }
            return RUS;
        }

        public String getLanguage() {
            return locale.getLanguage();
        }

        public Locale getLocale() {
            return locale;
        }
    }

}
