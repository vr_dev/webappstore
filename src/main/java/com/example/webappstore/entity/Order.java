package com.example.webappstore.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class Order implements Serializable {

    private static final long serialVersionUID = 7945020562538111893L;

    private long id;
    private Timestamp dateTime;
    private long statusId;
    private long userId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

}
