package com.example.webappstore.entity;

import java.io.Serializable;

public class Status implements Serializable {
    private static final long serialVersionUID = 2248522943128116980L;

    private long id;
    private String statusName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
