package com.example.webappstore.entity.type;

public enum UserRole {
    ADMIN(1, "administrator"),
    USER(2, "user");

    private final int id;
    private final String name;

    private UserRole(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static UserRole getTypeById(int id) {

        for (UserRole userRole : UserRole.values()) {
            if (userRole.id == id) {
                return userRole;
            }
        }

        throw new EnumConstantNotPresentException(UserRole.class, String.format("Role with id: %d is not found", id));
    }
}
