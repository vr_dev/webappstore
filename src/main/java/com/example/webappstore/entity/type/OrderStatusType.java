package com.example.webappstore.entity.type;

public enum OrderStatusType {
    PENDING(1, "Pending"),
    PAID(2, "Paid"),
    CANCELLED(3, "Cancelled");

    private final int id;
    private final String type;

    private OrderStatusType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public static OrderStatusType getTypeById(int id) {
        for (OrderStatusType orderStatusType : OrderStatusType.values()) {
            if (orderStatusType.id == id) {
                return orderStatusType;
            }
        }

        throw new EnumConstantNotPresentException(OrderStatusType.class, String.format("Status with id: %d is not found", id));
    }
}
