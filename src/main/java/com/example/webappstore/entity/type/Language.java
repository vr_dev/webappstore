package com.example.webappstore.entity.type;

public enum Language {
    RUS(1, new java.util.Locale("ru", "RU")),
    ENG(2, java.util.Locale.US);

    private int id;
    private final java.util.Locale locale;

    Language(int id, java.util.Locale locale) {
        this.id = id;
        this.locale = locale;
    }

    /**
     * Finds and returns the matching locale
     *
     * @param inputLanguage required locale
     * @return {@link Language#ENG} if locale is not found.
     */
    public static int getIdFromLanguage(String inputLanguage) {
        for (Language currentLocale: Language.values()) {
            if (currentLocale.locale.getLanguage().equals(inputLanguage)) {
                return currentLocale.getId();
            }
        }

        return RUS.getId();
    }

    public static Language getLocaleFromLanguage(String inputLanguage) {
        for (Language currentLocale: Language.values()) {
            if (currentLocale.locale.getLanguage().equals(inputLanguage)) {
                return currentLocale;
            }
        }
        return RUS;
    }

    public String getLanguage() {
        return locale.getLanguage();
    }

    public java.util.Locale getLocale() {
        return locale;
    }

    public int getId() {
        return id;
    }
}
