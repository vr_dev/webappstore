package com.example.webappstore.entity;

import com.example.webappstore.entity.type.UserRole;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 8924657847802703228L;
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private UserRole role;
    private boolean blocked;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = UserRole.getTypeById(role);;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public boolean isAdmin() {
        return role.equals(UserRole.ADMIN);
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
