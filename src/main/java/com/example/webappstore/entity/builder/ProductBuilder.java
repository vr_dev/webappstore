package com.example.webappstore.entity.builder;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.entity.ProductOptions;

import java.sql.Timestamp;

public interface ProductBuilder {
    public ProductBuilder setId(long id);
    public ProductBuilder setProductCategory(ProductCategory category);
    public ProductBuilder setName(String name);
    public ProductBuilder setBrand(String brand);
    public ProductBuilder setDescription(String description);
    public ProductBuilder setCreatedTime(Timestamp created);
    public ProductBuilder setPrice(Double price);
    public ProductBuilder setImageUrl(String imageUrl);
    public ProductBuilder setThumbUrl(String thumbUrl);
    public ProductBuilder setProductOptions(ProductOptions options);
    public Product build();

}
