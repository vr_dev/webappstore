package com.example.webappstore.entity.builder;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.entity.ProductOptions;

import java.sql.Timestamp;

public class ProductBuilderImpl implements ProductBuilder {
    Product product = new Product();
    public ProductBuilder setId(long id) {
        product.setId(id);
        return this;
    }

    @Override
    public ProductBuilder setProductCategory(ProductCategory category) {
        product.setCategory(category);
        return this;
    }

    public ProductBuilder setName(String name) {
        product.setName(name);
        return this;
    }

    @Override
    public ProductBuilder setBrand(String brand) {
        product.setBrand(brand);
        return this;
    }

    public ProductBuilder setDescription(String description) {
        product.setDescription(description);
        return this;
    }

    @Override
    public ProductBuilder setCreatedTime(Timestamp created) {
        product.setCreated(created);
        return this;
    }

    public ProductBuilder setPrice(Double price) {
        product.setPrice(price);
        return this;
    }

    public ProductBuilder setImageUrl(String imageUrl) {
        product.setImageUrl(imageUrl);
        return this;
    }

    @Override
    public ProductBuilder setThumbUrl(String thumbUrl) {
        product.setThumbUrl(thumbUrl);
        return this;
    }

    @Override
    public ProductBuilder setProductOptions(ProductOptions options) {
        product.setOptions(options);
        return this;
    }

    @Override
    public Product build() {
        return product;
    }

}
