package com.example.webappstore.entity.builder;

import com.example.webappstore.entity.ProductOptions;

import java.util.LinkedHashMap;

public class ProductOptionsBuilder {
    private ProductOptions options;

    public ProductOptionsBuilder() {
        this.options = new ProductOptions(new LinkedHashMap<>());
    }

    public ProductOptionsBuilder addOptionWithValue(String name, String value) {
        options.getAttributes().put(name, value);
        return this;
    }

    public ProductOptions build() {
        return options;
    }
}
