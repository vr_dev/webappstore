package com.example.webappstore.entity;

import java.io.Serializable;

public class ProductCategory implements Serializable {
    private static final long serialVersionUID = -8130539624873091423L;
    int id;
    private String name;
    private String thumbUrl;


    public ProductCategory() {
    }

    public ProductCategory(int id) {
        this.id = id;
    }

    public ProductCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public ProductCategory(int id, String name, String thumbUrl) {
        this.id = id;
        this.name = name;
        this.thumbUrl = thumbUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }
}
