package com.example.webappstore.entity;

import java.io.Serializable;
import java.util.*;

public class ProductOptions implements Serializable {
    private static final long serialVersionUID = 8670231347802703228L;

    private Map<String, String> attributes;

    public ProductOptions() {
    }

    public ProductOptions(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductOptions that = (ProductOptions) o;
        return attributes.equals(that.attributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributes);
    }
}

