package com.example.webappstore.util.constants;

public class CommandStorage {
        public static final String ROOT = "";
        public static final String ROOT_2 = "/";

        public static final String HOME = "/home";
        public static final String PRODUCT_CATALOG = "/productCatalog";
        public static final String REGISTRATION = "/registration";
        public static final String REGISTRATION_ADMIN = "/registrationAdmin";
        public static final String LOGIN = "/login";
        public static final String PRODUCT = "/product";
        public static final String ADD_PRODUCT_TO_CART = "/addProductToCart";
        public static final String CART = "/cart";
        public static final String DELETE_PRODUCT_FROM_CART = "/deleteProductFromCart";
        public static final String CREATE_PRODUCT = "/createProduct";
        public static final String UPDATE_PRODUCT = "/updateProduct";
        public static final String DELETE_PRODUCT = "/deleteProduct";
        public static final String ALL_USERS = "/allUsers";
        public static final String ACTIVATE_USER= "/activateUser";
        public static final String DEACTIVATE_USER  = "/deactivateUser";
        public static final String LOGOUT = "/logout";
        public static final String CREATE_ORDER = "/createOrder";
        public static final String ORDERS_ADMIN = "/ordersAdmin";
        public static final String ORDERS_USER = "/ordersUser";
        public static final String ORDER_DETAIL = "/orderDetail";
        public static final String CHANGE_STATUS = "/changeStatus";
        public static final String DELETE_ORDER_ADMIN_SERVICE = "/DeleteOrderAdminService";
        public static final String CHANGE_LANGUAGE = "/changeLanguage";
        public static final String ORDER = "/order";
}
