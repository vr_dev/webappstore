package com.example.webappstore.util.constants;

public class ConstantNames {
    public static final String ID = "id";
    public static final String PRODUCTID = "product.id";
    public static final String USER_ID = "userId";
    public static final String NAME = "name";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_ID = "category_id";
    public static final String BRAND = "brand";
    public static final String OPTION_NAME = "option_name";
    public static final String VALUE = "value";
    public static final String DESCRIPTION = "description";
    public static final String CREATE_TIME = "create_time";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String ROLE = "role_id";
    public static final String BLOCKED = "blocked";
    public static final String IMAGE_URL = "image_url";
    public static final String THUMB_URL = "thumb_url";
    public static final String PRODUCTS_IN_CART = "productsInCart";
    public static final String PRODUCTS = "products";
    public static final String OPTION_MAP = "optionsMap";
    public static final String TOTAL_PRODUCTS_COUNT = "totalProductsCount";
    public static final String CATEGORIES = "categories";
    public static final String PRODUCT = "product";
    public static final String PRODUCT_MAP = "productMap";
    public static final String SUM_OF_PRICE = "sumOfPrice";
    public static final String PRICE = "price";
    public static final String ID_USER ="user_id";
    public static final String PRODUCT_ID= "productId";
    public static final String USER = "user";
    public static final String USERS = "users";
    public static final String ERROR = "error";
    public static final String LOGIN = "login";
    public static final String ALL_USER = "allUsers";
    public static final String STATUS_PENDING = "pending";
    public static final String LANGUAGE_PARAMETER = "language";
    public static final String ORDER_ID = "orderId";
    public static final String STATUS_ID = "statusId";
    public static final String ORDERS = "orders";
    public static final String STATUSES = "statuses";
    public static final String CURRENT_STATUS = "currentStatus";
    public static final String STATUS = "status";
    public static final String ORDER_ID_TABLE = "order_id";
    public static final String USER_EMAIL = "user_email";
    public static final String TOTAL_PRICE = "totalPrice";
    public static final String STATUS_NAME = "name";
    public static final String FIRST_NAME_TABLE = "first_name";
    public static final String LAST_NAME_TABLE = "last_name";

    public static final String PRODUCT_ID_TABLE = "product_id";
    public static final String DATE_TIME = "date_time";
    public static final String ERROR_USER_BLOCKED = "You are blocked";
    public static final String ERROR_EMAIL_EXIST = "Email already exist";
    public static final String ERROR_EMAIL_FORMAT = "Not correct format of mail, please type it again";
    public static final String ERROR_PASSWORD_FORMAT = "Not correct format of password, please type it again";
    public static final String ERROR_EMAIL_OR_PASSWORD = "Email or password is wrong";

    public static final String LANGUAGE = "lang";

    public static final String CART_LIST = "cartList";
}

