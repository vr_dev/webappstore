package com.example.webappstore.service.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.ProductDao;
import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.entity.ProductOptions;
import com.example.webappstore.entity.builder.ProductOptionsBuilder;
import com.example.webappstore.entity.builder.ProductBuilderImpl;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.example.webappstore.util.constants.ConstantNames.LANGUAGE;

public class ProductServiceImpl implements ProductService {
    private final ProductDao dao;

    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());

    public ProductServiceImpl(ProductDao productDao) {
        this.dao = productDao;
    }

    @Override
    public Product getProductById(Long id, String lang) throws ServiceException {
        try {
            return dao.getProductById(id, lang);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void createProduct(HttpServletRequest request) throws ServiceException {
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, Product> productMap = getProductMapFromRequest(parameterMap);

        try {
            dao.createProduct(productMap);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }


    @Override
    public void updateProduct(HttpServletRequest request) throws ServiceException {
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, Product> productMap = getProductMapFromRequest(parameterMap);

        try {
            dao.updateProduct(productMap);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    private Map<String, Product> getProductMapFromRequest(Map<String, String[]> parameterMap) {
        long parameterCountRus = parameterMap.keySet().stream().filter(s -> s.contains("optionNameRus")).count();
        long parameterCountEng = parameterMap.keySet().stream().filter(s -> s.contains("optionNameEng")).count();

        ProductOptionsBuilder optionsBuilderRus = new ProductOptionsBuilder();
        ProductOptionsBuilder optionsBuilderEng = new ProductOptionsBuilder();

        for (int i = 1; i <= parameterCountRus; i++) {
            String optionNameRus = parameterMap.get("optionNameRus" + i)[0];
            String optionValueRus = parameterMap.get("optionValueRus" + i)[0];

            if (optionNameRus != "" && optionValueRus != "")
                optionsBuilderRus.addOptionWithValue(optionNameRus, optionValueRus);
        }

        for (int i = 1; i <= parameterCountEng; i++) {
            String optionNameEng = parameterMap.get("optionNameEng" + i)[0];
            String optionValueEng = parameterMap.get("optionValueEng" + i)[0];

            if (optionNameEng != "" && optionValueEng != "")
                optionsBuilderEng.addOptionWithValue(optionNameEng, optionValueEng);
        }

        ProductOptions optionsRus = optionsBuilderRus.build();

        long productId = 0l;
        if (parameterMap.containsKey("productId"))
            productId = Long.parseLong(parameterMap.get("productId")[0]);

        Product productRus = new ProductBuilderImpl()
                .setId(productId)
                .setName(parameterMap.get("nameRus")[0])
                .setBrand(parameterMap.get("brandRus")[0])
                .setDescription(parameterMap.get("descriptionRus")[0])
                .setImageUrl(parameterMap.get("imageUrl")[0])
                .setThumbUrl(parameterMap.get("thumbUrl")[0])
                .setPrice(Double.parseDouble(parameterMap.get("price")[0]))
                .setProductOptions(optionsRus)
                .setProductCategory(new ProductCategory(Integer.parseInt(parameterMap.get("productCategory")[0])))
                .build();


        ProductOptions optionsEng = optionsBuilderEng.build();

        Product productEng = new ProductBuilderImpl()
                .setId(productId)
                .setName(parameterMap.get("nameEng")[0])
                .setBrand(parameterMap.get("brandEng")[0])
                .setDescription(parameterMap.get("descriptionEng")[0])
                .setImageUrl(parameterMap.get("imageUrl")[0])
                .setThumbUrl(parameterMap.get("thumbUrl")[0])
                .setPrice(Double.parseDouble(parameterMap.get("price")[0]))
                .setProductOptions(optionsEng)
                .setProductCategory(new ProductCategory(Integer.parseInt(parameterMap.get("productCategory")[0])))
                .build();

        Map<String, Product> productMap = new LinkedHashMap<>();
        productMap.put("ru", productRus);
        productMap.put("en", productEng);

        return productMap;
    }

    @Override
    public void deleteProduct(long productId) throws ServiceException {
        try {
            dao.deleteProduct(productId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ProductCategory> getAllCategories(String language) throws ServiceException {
        try {
            return dao.getAllProductCategories(language);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public long getTotalProductCountByOptions(HttpServletRequest request) throws ServiceException {
        String language = (String) request.getSession().getAttribute(LANGUAGE);
        Map<String, String[]> parameterMap = request.getParameterMap();

        long count = 0L;
        try {
            count = dao.getTotalProductCountWithOptions(parameterMap, language);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return count;
    }

    @Override
    public List<Product> getAllProductByOptionAndCategory(HttpServletRequest request) throws ServiceException {
        String language = (String) request.getSession().getAttribute(LANGUAGE);
        Map<String, String[]> parameterMap = request.getParameterMap();

        List<Product> products = null;
        try {
            products = dao.getAllProductByOption(parameterMap, language);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return products;
    }

    @Override
    public Map<String, List<String>> getAllOptionsByProductCategory(HttpServletRequest request) throws ServiceException {
        long categoryId = Long.parseLong(request.getParameter("productCategory"));
        String language = (String) request.getSession().getAttribute(LANGUAGE);

        try {
            return dao.getAllOptionsByProductCategory(categoryId, language);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }
}
