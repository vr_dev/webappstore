package com.example.webappstore.service.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.UserDao;
import com.example.webappstore.entity.User;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

public class UserServiceImpl implements UserService {
    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());
    UserDao dao;

    public UserServiceImpl(UserDao dao) {
        this.dao = dao;
    }

    @Override
    public void addUser(User user) throws ServiceException {
        try {
            dao.addUser(user);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<User> getUsers() throws ServiceException {
        try {
            return dao.getUsers();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public User getUserByLoginPassword(String login, String password) throws ServiceException {
        try {
            return dao.getUserByLoginPassword(login, password);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void ActivateUser(long userId, boolean isBlocked) throws ServiceException {
        try {
            dao.activateUser(userId, isBlocked);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public boolean isEmailExist(String email) throws ServiceException {
        try {
            return dao.isEmailExist(email);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }
}
