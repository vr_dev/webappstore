package com.example.webappstore.service.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.StatusDao;
import com.example.webappstore.entity.Status;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.StatusService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

public class StatusServiceImpl implements StatusService {
    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());
    StatusDao dao;

    public StatusServiceImpl(StatusDao dao) {
        this.dao = dao;
    }

    @Override
    public Long getIdByStatusName(String statusName) throws ServiceException {
        try {
            return dao.getIdByStatusName(statusName);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Status> getAllStatuses() throws ServiceException {
        try {
            return dao.getAllStatuses();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Status getStatusByOrderId(Long orderId) throws ServiceException {
        try {
            return dao.getStatusByOrderId(orderId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }
}
