package com.example.webappstore.service.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.OrderDao;
import com.example.webappstore.entity.Order;
import com.example.webappstore.entity.OrderItem;
import com.example.webappstore.entity.Product;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.OrderService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());
    OrderDao dao;

    public OrderServiceImpl(OrderDao dao) {
        this.dao = dao;
    }

    @Override
    public void createOrder(long userId, List<Product> productList) throws ServiceException {
        try {
            dao.createOrder(userId, productList);
            //todo change exception multiply
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long takeLastID() throws ServiceException {
        try {
            return dao.takeLastID();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public ArrayList<Order> getAllOrder() throws ServiceException {
        try {
            return dao.getAllOrder();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public ArrayList<ArrayList<String>> getFrom4Tables() throws ServiceException {
        try {
            return dao.getFrom4Tables();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void changeOrderStatus(Long orderId, Long statusId) throws ServiceException {
        try {
            dao.changeOrderStatus(orderId, statusId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public ArrayList<ArrayList<String>> getOrderByUserId(Long userId) throws ServiceException {
        try {
            return dao.getOrderByUserId(userId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public void deleteOrderById(Long orderId) throws ServiceException {
        try {
            dao.deleteOrderById(orderId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public Order getOrderById(Long orderId) throws ServiceException {
        try {
            return dao.getOrderById(orderId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void createOrderItem(OrderItem orderItem) throws ServiceException {
        try {
            dao.createOrderItem(orderItem);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public ArrayList<Long> getProductsIdFromOrderItem(Long orderId) throws ServiceException {
        try {
            return dao.getProductsIdFromOrderItem(orderId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }
}
