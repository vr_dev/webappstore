package com.example.webappstore.service.interfaces;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.service.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Map;

public interface ProductService {
    Product getProductById(Long id, String lang) throws ServiceException;

    void createProduct(HttpServletRequest request) throws ServiceException;

    void updateProduct(HttpServletRequest request)  throws ServiceException;

    void deleteProduct(long productId) throws ServiceException;

    List<ProductCategory> getAllCategories(String language) throws ServiceException;

    List<Product> getAllProductByOptionAndCategory(HttpServletRequest request) throws ServiceException;

    Map<String, List<String>> getAllOptionsByProductCategory(HttpServletRequest request) throws ServiceException;

    long getTotalProductCountByOptions(HttpServletRequest request) throws ServiceException;
}
