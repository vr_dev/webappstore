package com.example.webappstore.service.interfaces;

import com.example.webappstore.entity.User;
import com.example.webappstore.service.exception.ServiceException;

import java.util.List;

public interface UserService {
    void addUser(User user) throws ServiceException;

    List<User> getUsers() throws ServiceException;

    User getUserByLoginPassword(String login, String password) throws ServiceException;

    void ActivateUser(long userId, boolean isBlocked) throws ServiceException;

    boolean isEmailExist(String email) throws ServiceException;

}
