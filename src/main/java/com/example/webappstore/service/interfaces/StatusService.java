package com.example.webappstore.service.interfaces;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.entity.Status;
import com.example.webappstore.service.exception.ServiceException;

import java.util.List;

public interface StatusService {
    Long getIdByStatusName(String statusName) throws ServiceException;

    List<Status> getAllStatuses() throws ServiceException;

    Status getStatusByOrderId(Long orderId) throws ServiceException;
}
