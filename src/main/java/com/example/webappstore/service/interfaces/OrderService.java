package com.example.webappstore.service.interfaces;

import com.example.webappstore.entity.Order;
import com.example.webappstore.entity.OrderItem;
import com.example.webappstore.entity.Product;
import com.example.webappstore.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

public interface OrderService {
    void createOrder(long userId, List<Product> products) throws ServiceException;

    Long takeLastID() throws ServiceException;

    ArrayList<Order> getAllOrder() throws ServiceException;

    ArrayList<ArrayList<String>> getFrom4Tables() throws ServiceException;

    void changeOrderStatus(Long orderId, Long statusId) throws ServiceException;

    ArrayList<ArrayList<String>> getOrderByUserId(Long userId) throws ServiceException;

    void deleteOrderById(Long orderId) throws ServiceException;

    Order getOrderById(Long orderId) throws ServiceException;

    void createOrderItem(OrderItem orderItem) throws ServiceException;

    ArrayList<Long> getProductsIdFromOrderItem(Long orderId) throws ServiceException;
}
