package com.example.webappstore.service.factory;

import com.example.webappstore.dao.impl.StatusDaoImpl;
import com.example.webappstore.dao.interfaces.StatusDao;
import com.example.webappstore.entity.Status;

import com.example.webappstore.entity.type.OrderStatusType;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.OrderService;
import com.example.webappstore.service.interfaces.StatusService;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

import static com.example.webappstore.util.constants.ConstantNames.STATUS_PENDING;

public class StatusFactory {
    public static StatusFactory instance = new StatusFactory();

    public Status fillStatus() {
        Status status = new Status();
        status.setId(OrderStatusType.PENDING.getId());
        status.setStatusName(OrderStatusType.PENDING.getType());

        return status;
    }

    public static StatusFactory getInstance() {
        if (instance == null) {
            instance = new StatusFactory();
        }
        return instance;
    }
}
