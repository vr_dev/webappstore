package com.example.webappstore.service.factory;

import com.example.webappstore.entity.Order;
import com.example.webappstore.entity.OrderItem;

import jakarta.servlet.http.HttpServletRequest;

public class OrderItemFactory {
    public static OrderItemFactory instance = new OrderItemFactory();

    public OrderItem fillOrderItem(long productId, double price, Order order){
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(productId);
        orderItem.setPrice(price);
        orderItem.setOrderId(order.getId());
        //todo implement quantity
        return orderItem;
    }
    public static OrderItemFactory getInstance() {
        if (instance == null) {
            instance = new OrderItemFactory();
        }
        return instance;
    }
}
