package com.example.webappstore.service.factory;

import com.example.webappstore.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import jakarta.servlet.http.HttpServletRequest;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.*;
public class UserFactory {
    private static UserFactory instance = new UserFactory();

    public User fillUser(HttpServletRequest request, int role) {
        User newUser = new User();
        newUser.setFirstName(request.getParameter(FIRST_NAME));
        newUser.setLastName(request.getParameter(LAST_NAME));
        newUser.setEmail(request.getParameter(EMAIL));
        String password = request.getParameter(PASSWORD);
        String securedPassword = DigestUtils.md5Hex(password);
        newUser.setPassword(securedPassword);
        newUser.setRole(role);
        return newUser;
    }

    public static UserFactory getInstance() {
        if (instance == null) {
            instance = new UserFactory();
        }
        return instance;
    }
}
