package com.example.webappstore.service.factory;

import com.example.webappstore.entity.Order;
import com.example.webappstore.entity.Status;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class OrderFactory {
    private static OrderFactory instance = new OrderFactory();

    public Order fillOrder(Status status, long userId) {
        Order order = new Order();
        LocalDateTime now = LocalDateTime.now();
        Timestamp dateTime = Timestamp.valueOf(now);
        order.setDateTime(dateTime);
        order.setStatusId(status.getId());
        order.setUserId(userId);
        return order;

    }

    public static OrderFactory getInstance() {
        if (instance == null) {
            instance = new OrderFactory();
        }
        return instance;
    }

}
