package com.example.webappstore.service.factory;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.builder.ProductBuilderImpl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import static com.example.webappstore.util.constants.ConstantNames.*;

public class ProductFactory {
    private static ProductFactory instance = new ProductFactory();



    public Product fillProduct(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return new ProductBuilderImpl().
                setName(request.getParameter(NAME)).
                setDescription(request.getParameter(DESCRIPTION)).
                setImageUrl(request.getParameter(IMAGE_URL)).
                setPrice(Double.parseDouble(request.getParameter(PRICE))).
                build();
    }

    public Product updateProduct(HttpServletRequest request){
        return new ProductBuilderImpl().
                setName(request.getParameter(NAME)).
                setDescription(request.getParameter(DESCRIPTION)).
                setImageUrl(request.getParameter(IMAGE_URL)).
                setPrice(Double.parseDouble(request.getParameter(PRICE))).
                setId(Long.parseLong(request.getParameter(PRODUCT_ID)))
                .build();
    }

    public static ProductFactory getInstance() {
        if (instance == null) {
            instance = new ProductFactory();
        }
        return instance;
    }
}
