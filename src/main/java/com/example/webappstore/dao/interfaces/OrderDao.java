package com.example.webappstore.dao.interfaces;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.entity.Order;
import com.example.webappstore.entity.OrderItem;
import com.example.webappstore.entity.Product;

import java.util.ArrayList;
import java.util.List;

public interface OrderDao {
    void createOrder(long userId, List<Product> productList) throws DAOException;

    Long takeLastID() throws DAOException;

    ArrayList<Order> getAllOrder() throws DAOException;

    ArrayList<ArrayList<String>> getFrom4Tables() throws DAOException;

    void changeOrderStatus(Long orderId, Long statusId) throws DAOException;

    ArrayList<ArrayList<String>> getOrderByUserId(Long userId) throws DAOException;

    void deleteOrderById(Long orderId) throws DAOException;

    Order getOrderById(Long orderId) throws DAOException;

    void createOrderItem(OrderItem orderItem) throws DAOException;

    ArrayList<Long> getProductsIdFromOrderItem(Long orderId) throws DAOException;
}
