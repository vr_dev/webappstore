package com.example.webappstore.dao.interfaces;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.entity.User;

import java.util.List;

public interface UserDao {
    void addUser(User user) throws DAOException;

    List<User> getUsers() throws DAOException;

    User getUserByLoginPassword(String login, String password) throws DAOException;

    void activateUser(long userId, boolean isBlocked) throws DAOException;

    boolean isEmailExist(String email) throws DAOException;

}
