package com.example.webappstore.dao.interfaces;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.entity.Status;

import java.util.List;

public interface StatusDao {
    Long getIdByStatusName(String statusName) throws DAOException;

    List<Status> getAllStatuses() throws DAOException;

    Status getStatusByOrderId(Long orderId) throws DAOException;
}
