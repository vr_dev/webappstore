package com.example.webappstore.dao.interfaces;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;

import java.util.List;
import java.util.Map;

public interface ProductDao {

    Map<String, List<String>> getAllOptionsByProductCategory(long catId, String lang) throws DAOException;

    List<Product> getAllProductByOption(Map<String, String[]> parameterMap, String lang) throws DAOException;

    long getTotalProductCountWithOptions(Map<String, String[]> parameterMap, String lang) throws DAOException;

    Product getProductById(Long id, String lang) throws DAOException;

    void createProduct(Map<String, Product> productMap) throws DAOException;

    void updateProduct(Map<String, Product> productMap) throws DAOException;


    void deleteProduct(long productId) throws DAOException;

    List<ProductCategory> getAllProductCategories(String language) throws DAOException;
}
