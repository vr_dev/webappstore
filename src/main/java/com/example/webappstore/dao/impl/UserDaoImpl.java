package com.example.webappstore.dao.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.UserDao;
import com.example.webappstore.entity.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.*;


public class UserDaoImpl extends AbstractDAO implements UserDao{
    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());

//    private static final String INSERT_INTO_USERS = "INSERT INTO user (first_name, last_name, email, password, role_id, blocked) VALUES" +
//            "(?, ?, ?, ?, ?, ?)";
    private static final String INSERT_INTO_USERS = "INSERT INTO user (first_name, last_name, email, password, role_id) VALUES" +
            "(?, ?, ?, ?, ?)";
    private static final String GET_USER_BY_LOGIN_PASSWORD = "SELECT id, first_name, last_name, email, password, role_id, blocked FROM USER " +
            "WHERE email = ? AND password = ?";
    private static final String GET_ALL_USERS = "SELECT * FROM USER WHERE role_id = (SELECT id FROM role WHERE name = 'user')";
    private static final String UPDATE_USER_ACTIVITY = "UPDATE USER SET blocked = ? WHERE id = ?";
    private static final String CHECK_LOGIN = "SELECT * FROM user WHERE email = ?";

    public UserDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public void addUser(User user) throws DAOException {
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(INSERT_INTO_USERS)) {

                pstmt.setString(1, user.getFirstName());
                pstmt.setString(2, user.getLastName());
                pstmt.setString(3, user.getEmail());
                pstmt.setString(4, user.getPassword());
                pstmt.setInt(5, user.getRole().getId());
//                pstmt.setBoolean(6, user.isActive());
                int count = pstmt.executeUpdate();
                if (count != 1)
                    throw new SQLException("Insert updated " + count + " rows");
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    public User getUserByLoginPassword(String login, String password) throws DAOException {
        User user = null;
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement preparedStatement = con.prepareStatement(GET_USER_BY_LOGIN_PASSWORD)) {
                preparedStatement.setString(1, login);
                preparedStatement.setString(2, password);

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getLong(ID));
                    user.setFirstName(resultSet.getString(FIRST_NAME_TABLE));
                    user.setLastName(resultSet.getString(LAST_NAME_TABLE));
                    user.setEmail(resultSet.getString(EMAIL));
                    user.setPassword(resultSet.getString(PASSWORD));
                    user.setRole(resultSet.getInt(ROLE));
                    user.setBlocked(resultSet.getBoolean(BLOCKED));
                }

//                resultSet.close();
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return user;
    }

    public boolean isEmailExist(String email) throws DAOException {
        boolean isExist = false;
        Connection con = null;
        try {
            con = getConnection(false);
            PreparedStatement preparedStatement = con.prepareStatement(CHECK_LOGIN);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                isExist = true;
            }

            resultSet.close();
            preparedStatement.close();
            con.commit();

        } catch (Exception e) {
            LOGGER.error(e);
            rollback(con);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return isExist;
    }

    public List<User> getUsers() throws DAOException {
        List<User> users = new ArrayList<>();
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement preparedStatement = con.prepareStatement(GET_ALL_USERS)) {


                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getLong(ID));
                    user.setFirstName(resultSet.getString(FIRST_NAME_TABLE));
                    user.setLastName(resultSet.getString(LAST_NAME_TABLE));
                    user.setEmail(resultSet.getString(EMAIL));
                    user.setPassword(resultSet.getString(PASSWORD));
                    user.setRole(resultSet.getInt(ROLE));
                    user.setBlocked(resultSet.getBoolean(BLOCKED));
                    users.add(user);
                }

                resultSet.close();
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return users;

    }

    public void activateUser(long userId, boolean isBlocked) throws DAOException {
        Connection con = null;
        try{
            con = getConnection(false);
            int count;
            try (PreparedStatement preparedStatement = con.prepareStatement(UPDATE_USER_ACTIVITY)) {

                preparedStatement.setBoolean(1, isBlocked);
                preparedStatement.setLong(2, userId);

                count = preparedStatement.executeUpdate();
            }
            if (count != 1) {
                throw new SQLException("Updated " + count + " rows");
            }
            con.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
            rollback(con);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

}