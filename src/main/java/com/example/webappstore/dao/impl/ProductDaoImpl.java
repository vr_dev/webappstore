package com.example.webappstore.dao.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.ProductDao;
import com.example.webappstore.dao.mapper.RowMapper;
import com.example.webappstore.dao.mapper.RowMapperFactory;
import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.entity.ProductOptions;
import com.example.webappstore.entity.builder.ProductOptionsBuilder;
import com.example.webappstore.entity.type.Language;
import com.example.webappstore.validator.ValidateXSS;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

import static com.example.webappstore.util.constants.ConstantNames.OPTION_NAME;
import static com.example.webappstore.util.constants.ConstantNames.VALUE;

public class ProductDaoImpl extends AbstractDAO implements ProductDao {

    private static final Integer DEFAULT_RECORDS_PER_PAGE = 9;
    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());

    private static final String GET_ALL_PRODUCT_CATEGORIES =
            "SELECT product_category.id, pci18n.category_name, product_category.thumb_url FROM product_category \n" +
                    "JOIN product_category_i18n pci18n ON product_category.id = pci18n.category_id \n " +
                    "WHERE pci18n.lang_id = ?";
    private static final String GET_PRODUCT =
            "SELECT * FROM product " +
                    "JOIN product_i18n pi18n on product.id = pi18n.product_id \n" +
                    "WHERE product.id = ? AND pi18n.lang_id = ?";

    private static final String GET_OPTIONS_FOR_PRODUCT =
            "SELECT po.id, oi18n.option_name, poi18n.value FROM product_options po " +
                    "JOIN product_options_i18n poi18n ON po.id = poi18n.product_options_id " +
                    "JOIN options o ON po.options_id = o.id\n" +
                    "JOIN options_i18n oi18n ON  o.id = oi18n.options_id AND oi18n.lang_id = poi18n.lang_id " +
                    "JOIN product p ON po.product_id = p.id " +
                    "WHERE p.id = ? AND oi18n.lang_id = ?";
    private static final String DELETE_PRODUCT = "DELETE FROM product WHERE id = ?";
    private static final String CLEAR_UNUSED_OPTIONS = "DELETE FROM options WHERE options.id NOT IN (SELECT po.options_id FROM product_options po)";

    public ProductDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public Product getProductById(Long id, String lang) throws DAOException {
        Product product = null;
        Connection con = null;

        RowMapper<Product> productMapper = RowMapperFactory.getInstance().getProductRowMapper();
        try {
            con = getConnection();
            try (PreparedStatement productStmt = con.prepareStatement(GET_PRODUCT)) {
                productStmt.setLong(1, id);
                productStmt.setLong(2, Language.getIdFromLanguage(lang));
                ResultSet rs = productStmt.executeQuery();
                while (rs.next()) {
                    product = productMapper.map(rs);
                }
            }

            if (product != null) {
                ProductOptions options = getOptionsByProductId(id, lang);
                product.setOptions(options);
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return product;
    }

    private ProductOptions getOptionsByProductId(Long id, String lang) throws SQLException {
        ProductOptionsBuilder builder = new ProductOptionsBuilder();
        try (Connection con = getConnection();
             PreparedStatement optionsStmt = con.prepareStatement(GET_OPTIONS_FOR_PRODUCT)) {
            optionsStmt.setLong(1, id);
            optionsStmt.setLong(2, Language.getIdFromLanguage(lang));
            ResultSet rs = optionsStmt.executeQuery();

            while (rs.next()) {
                builder.addOptionWithValue(rs.getString(OPTION_NAME), rs.getString(VALUE));
            }
        }

        return builder.build();
    }


    @Override
    public Map<String, List<String>> getAllOptionsByProductCategory(long catId, String lang) throws DAOException {
        LinkedHashMap<String, List<String>> options = new LinkedHashMap<>();

        try {
            Connection con = getConnection();
            try (PreparedStatement statement = con.prepareStatement("SELECT DISTINCT oi18n.option_name, poi18n.value\n" +
                    "FROM product\n" +
                    "         JOIN product_category ON product.category_id = product_category.id \n" +
                    "         JOIN options o on product_category.id = o.category_id \n" +
                    "         JOIN options_i18n oi18n on o.id = oi18n.options_id \n" +
                    "         JOIN product_options po on product.id = po.product_id \n" +
                    "         JOIN product_options_i18n poi18n on po.id = poi18n.product_options_id AND oi18n.lang_id = poi18n.lang_id \n" +
                    "WHERE product_category.id = ? AND po.options_id = o.id AND oi18n.lang_id = ?")) {

                statement.setLong(1, catId);
                statement.setInt(2, Language.getIdFromLanguage(lang));

                ResultSet rs = statement.executeQuery();

                while (rs.next()) {
                    String optionName = rs.getString(1);
                    String optionValue = rs.getString(2);
                    if (options.containsKey(optionName)) {
                        options.get(optionName).add(optionValue);
                    } else {
                        ArrayList<String> list = new ArrayList<>();
                        list.add(optionValue);
                        options.put(optionName, list);
                    }
                }

            }

        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }

        return options;
    }

    @Override
    public List<Product> getAllProductByOption(Map<String, String[]> parameterMap, String lang) throws DAOException {
        List<Product> products = new ArrayList<>();
        Connection con = null;

        String selectQuery = getProductQueryWithParameters(parameterMap, lang);

        RowMapper<Product> productMapper = RowMapperFactory.getInstance().getProductRowMapper();
        try {
            con = getConnection();

            try (Statement statement = con.createStatement()) {
                if (statement.execute(selectQuery)) {
                    ResultSet rs = statement.getResultSet();
                    while (rs.next()) {
                        Product product = productMapper.map(rs);
                        products.add(product);
                    }
                }
            }


        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }

        return products;
    }

    private String getProductQueryWithParameters(Map<String, String[]> parameterMap, String language) {
        String initQuery = "SELECT * FROM product \n" +
                "         JOIN product_i18n pi18n on product.id = pi18n.product_id \n" +
                "         JOIN options o on product.category_id = o.category_id \n" +
                "         JOIN options_i18n oi18n on o.id = oi18n.options_id \n" +
                "         JOIN product_options po on product.id = po.product_id \n" +
                "         JOIN product_options_i18n poi18n on po.id = poi18n.product_options_id \n";

        QueryBuilder builder = new QueryBuilder(initQuery, parameterMap);

        Map<String, String> sortingMap = new HashMap<>();
        sortingMap.put("titleUp", "pi18n.name ASC");
        sortingMap.put("titleDown", "pi18n.name DESC");
        sortingMap.put("priceUp", "product.price ASC");
        sortingMap.put("priceDown", "product.price DESC");
        sortingMap.put("latest", "product.create_time DESC");

        builder.withCategory("product.category_id")
                .withPrice("price")
                .withBrand("brand")
                .withLanguage("pi18n.lang_id", language)
                .withProductOptionValue("poi18n.value")
                .withGroupBy("product.id")
                .withSortingBy("product.id", sortingMap)
                .withPagination();

        return builder.build();
    }

    private String getProductCountQueryWithParametersNoLimit(Map<String, String[]> parameterMap, String language) {
        String initQuery = "SELECT COUNT(DISTINCT product.id) FROM product\n" +
                "         JOIN product_i18n pi18n on product.id = pi18n.product_id \n" +
                "         JOIN options o on product.category_id = o.category_id \n" +
                "         JOIN options_i18n oi18n on o.id = oi18n.options_id \n" +
                "         JOIN product_options po on product.id = po.product_id \n" +
                "         JOIN product_options_i18n poi18n on po.id = poi18n.product_options_id \n";

        QueryBuilder builder = new QueryBuilder(initQuery, parameterMap);

        builder.withCategory("product.category_id")
                .withPrice("price")
                .withBrand("brand")
                .withLanguage("pi18n.lang_id", language)
                .withProductOptionValue("poi18n.value");

        return builder.build();
    }

    @Override
    public long getTotalProductCountWithOptions(Map<String, String[]> parameterMap, String lang) throws DAOException {
        Connection con = null;
        long count = 0;
        try {
            con = getConnection();

            String query = getProductCountQueryWithParametersNoLimit(parameterMap, lang);

            try (Statement pstmt = con.createStatement()) {
                ResultSet rs = pstmt.executeQuery(query);
                while (rs.next()) {
                    count = rs.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }

        return count;
    }

    @Override
    public void updateProduct(Map<String, Product> productMap) throws DAOException {
        Connection con = null;

        Product productRus = productMap.get("ru");
        Product productEng = productMap.get("en");

        try {
            con = getConnection(false);
            PreparedStatement productStatement = con.prepareStatement("UPDATE product SET price = ?, image_url= ?, thumb_url= ? WHERE id = ?");

            productStatement.setDouble(1, productRus.getPrice());
            productStatement.setString(2, productRus.getImageUrl());
            productStatement.setString(3, productRus.getThumbUrl());
            productStatement.setLong(4, productRus.getId());


            PreparedStatement productI18nRus = con.prepareStatement("UPDATE product_i18n SET name = ?, brand = ?, description = ? WHERE product_id = ? AND lang_id = ?");
            productI18nRus.setString(1, productRus.getName());
            productI18nRus.setString(2, productRus.getBrand());
            productI18nRus.setString(3, productRus.getDescription());
            productI18nRus.setLong(4, productRus.getId());
            productI18nRus.setLong(5, Language.getIdFromLanguage("ru"));

            PreparedStatement productI18nEng = con.prepareStatement("UPDATE product_i18n SET name = ?, brand = ?, description = ? WHERE product_id = ? AND lang_id = ?");
            productI18nEng.setString(1, productEng.getName());
            productI18nEng.setString(2, productEng.getBrand());
            productI18nEng.setString(3, productEng.getDescription());
            productI18nEng.setLong(4, productEng.getId());
            productI18nEng.setLong(5, Language.getIdFromLanguage("en"));

            productStatement.execute();
            productI18nRus.execute();
            productI18nEng.execute();

            if (checkIfOptionsWereUpdated(productRus, con, Language.getIdFromLanguage("ru")) ||
                    checkIfOptionsWereUpdated(productEng, con, Language.getIdFromLanguage("en"))) {
                Statement stmt = con.createStatement();

                long productId = productRus.getId();
                int productCategoryId = productRus.getCategory().getId();

                Map<String, String> optionRus = productRus.getOptions().getAttributes();
                Map<String, String> optionEng = productEng.getOptions().getAttributes();

                if (stmt.executeUpdate("DELETE FROM product_options WHERE product_options.product_id = " + productId) != 0) {
                    addOptions(con, productId, productCategoryId, optionRus, optionEng);
                }
            }

            con.commit();
        } catch (Exception e) {
            rollback(con);
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    private boolean checkIfOptionsWereUpdated(Product product, Connection con, int languageId) throws SQLException {
        boolean hasChanges = false;

        Map<String, String> optionsProduct;
        ResultSet ors;
        try (PreparedStatement optionStmt = con.prepareStatement("SELECT o.category_id, o.id,oi18n.id, oi18n.option_name,po.id, poi18n.id, poi18n.value, oi18n.lang_id FROM options o " +
                "JOIN options_i18n oi18n ON o.id = oi18n.options_id " +
                "JOIN product_options po on o.id = po.options_id AND product_id = ? AND lang_id = ? " +
                "JOIN product_options_i18n poi18n on po.id = poi18n.product_options_id AND poi18n.lang_id = ?")) {

            optionStmt.setLong(1, product.getId());
            optionStmt.setInt(2, languageId);
            optionStmt.setInt(3, languageId);

            if (!optionStmt.execute()) {
                throw new DAOException("Can't get data from product option ");
            }

            optionsProduct = product.getOptions().getAttributes();

            ors = optionStmt.getResultSet();

            while (ors.next()) {
                String optionName = ors.getString("oi18n.option_name");
                String optionValue = ors.getString("poi18n.value");

                if (optionsProduct.containsKey(optionName)) {
                    if (!optionsProduct.get(optionName).equals(optionValue))
                        hasChanges = true;
                } else {
                    hasChanges = true;
                }
            }
        }

        return hasChanges;
    }

    @Override
    public void createProduct(Map<String, Product> productMap) throws DAOException {
        Connection con = null;

        Product productRus = productMap.get("ru");
        Product productEng = productMap.get("en");

        int productCategoryId = productRus.getCategory().getId();
        double price = productRus.getPrice();
        String url = productRus.getImageUrl();
        String thumb = productRus.getThumbUrl();

        try {
            con = getConnection(false);


            String productQuery = "INSERT INTO product (category_id, price, image_url, thumb_url) VALUES (?,?,?,?)";

            long productId = 0;
            PreparedStatement productStatement = con.prepareStatement(productQuery, Statement.RETURN_GENERATED_KEYS);
            productStatement.setInt(1, productCategoryId);
            productStatement.setDouble(2, price);
            productStatement.setString(3, url);
            productStatement.setString(4, thumb);

            if (productStatement.executeUpdate() != 0) {
                ResultSet rs = productStatement.getGeneratedKeys();
                if (rs.next()) {
                    productId = rs.getLong(1);
                }
            } else {
                throw new SQLException("can't add product in db");
            }

            String productQueryRus = "INSERT INTO product_i18n (product_id, name, brand, description, lang_id) VALUES (?,?,?,?,?)";
            PreparedStatement productI18nRus = con.prepareStatement(productQueryRus, Statement.RETURN_GENERATED_KEYS);

            String productQueryUkr = "INSERT INTO product_i18n (product_id, name, brand, description, lang_id) VALUES (?,?,?,?,?)";
            PreparedStatement productI18nEng = con.prepareStatement(productQueryUkr, Statement.RETURN_GENERATED_KEYS);

            productI18nRus.setLong(1, productId);
            productI18nRus.setString(2, productRus.getName());
            productI18nRus.setString(3, productRus.getBrand());
            productI18nRus.setString(4, productRus.getDescription());
            productI18nRus.setInt(5, Language.getIdFromLanguage("ru"));

            productI18nEng.setLong(1, productId);
            productI18nEng.setString(2, productEng.getName());
            productI18nEng.setString(3, productEng.getBrand());
            productI18nEng.setString(4, productEng.getDescription());
            productI18nEng.setInt(5, Language.getIdFromLanguage("en"));

            if (productI18nRus.executeUpdate() == 0) {
                throw new SQLException("can't add product_i18n in db");
            }

            if (productI18nEng.executeUpdate() == 0) {
                throw new SQLException("can't add product_i18n in db");
            }


            Map<String, String> optionRus = productRus.getOptions().getAttributes();
            Map<String, String> optionEng = productEng.getOptions().getAttributes();

            if (optionRus.size() != optionEng.size()) {
                throw new IllegalStateException("options size doesn't match");
            }

            addOptions(con, productId, productCategoryId, optionRus, optionEng);

            con.commit();

        } catch (SQLException ex) {
            rollback(con);
            throw new DAOException(ex);
        } finally {
            closeConnection(con);
        }
    }

    private void addOptions(Connection con, long productId, int productCat, Map<String, String> optionRus, Map<String, String> optionEng) throws DAOException {
        Map.Entry<String, String>[] rus = optionRus.entrySet().toArray(new Map.Entry[0]);
        Map.Entry<String, String>[] eng = optionEng.entrySet().toArray(new Map.Entry[1]);

        int optionId = 0;
        int optionCount = optionRus.size();

        for (int i = 0; i < optionCount; i++) {

            try (PreparedStatement ps3 = con.prepareStatement(
                    "SELECT options.id FROM options " +
                            "JOIN options_i18n ON options.id = options_i18n.options_id " +
                            "WHERE options_i18n.option_name = ? AND category_id = ?")) {

                ps3.setString(1, rus[i].getKey());
                ps3.setInt(2, productCat);
                ResultSet rs2 = ps3.executeQuery();

                if (rs2.next()) {
                    optionId = (int) rs2.getLong(1);
                } else {
                    try (PreparedStatement optionStatement = con.prepareStatement(
                            "INSERT INTO options (category_id) VALUES (?)",
                            Statement.RETURN_GENERATED_KEYS);

                         PreparedStatement optionStatementRus = con.prepareStatement(
                                 "INSERT INTO options_i18n (options_id, option_name, lang_id) VALUES (?,?,1)",
                                 Statement.RETURN_GENERATED_KEYS);

                         PreparedStatement optionStatementEng = con.prepareStatement(
                                 "INSERT INTO options_i18n (options_id, option_name, lang_id) VALUES (?,?,2)",
                                 Statement.RETURN_GENERATED_KEYS)) {

                        optionStatement.setInt(1, productCat);

                        if (optionStatement.executeUpdate() != 0) {
                            ResultSet gk = optionStatement.getGeneratedKeys();

                            if (gk.next()) {
                                optionId = gk.getInt(1);
                            }
                        }

                        optionStatementRus.setInt(1, optionId);
                        optionStatementRus.setString(2, rus[i].getKey());

                        optionStatementEng.setInt(1, optionId);
                        optionStatementEng.setString(2, eng[i].getKey());

                        optionStatementRus.executeUpdate();
                        optionStatementEng.executeUpdate();
                    }
                }

                PreparedStatement productOptions = con.prepareStatement("INSERT INTO product_options (product_id, options_id) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
                productOptions.setLong(1, productId);
                productOptions.setInt(2, optionId);

                long productOptionsId = 0;

                if (productOptions.executeUpdate() != 0) {
                    ResultSet rs = productOptions.getGeneratedKeys();
                    if (rs.next()) {
                        productOptionsId = rs.getLong(1);
                    }
                } else {
                    throw new DAOException("can't add product in db");
                }

                PreparedStatement productOptionsRus = con.prepareStatement("INSERT INTO product_options_i18n (product_options_id, value, lang_id) VALUES (?,?,1)");
                productOptionsRus.setLong(1, productOptionsId);
                productOptionsRus.setString(2, rus[i].getValue());
                productOptionsRus.executeUpdate();

                PreparedStatement productOptionsEng = con.prepareStatement("INSERT INTO product_options_i18n (product_options_id, value, lang_id) VALUES (?,?,2)");
                productOptionsEng.setLong(1, productOptionsId);
                productOptionsEng.setString(2, eng[i].getValue());

                productOptionsEng.executeUpdate();


            } catch (SQLException ex) {
                throw new DAOException(ex);
            }
        }
    }

    public void deleteProduct(long productId) throws DAOException {
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement preparedStatement = con.prepareStatement(DELETE_PRODUCT)) {
                preparedStatement.setLong(1, productId);

                if (preparedStatement.executeUpdate() != 0) {
                    Statement stmt = con.createStatement();
                    stmt.execute(CLEAR_UNUSED_OPTIONS);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    @Override
    public List<ProductCategory> getAllProductCategories(String language) throws DAOException {
        List<ProductCategory> categories = new ArrayList<>();
        Connection con = null;
        RowMapper<ProductCategory> productCategoryMapper = RowMapperFactory.getInstance().getProductCategoryRowMapper();
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(GET_ALL_PRODUCT_CATEGORIES)) {
                pstmt.setInt(1, Language.getIdFromLanguage(language));
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    ProductCategory product = productCategoryMapper.map(rs);
                    categories.add(product);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }

        return categories;
    }


    protected static class QueryBuilder {
        private final Map<String, String[]> parameterMap;
        private StringBuilder query;

        public QueryBuilder(String initQuery, Map<String, String[]> parameterMap) {
            this.parameterMap = parameterMap;
            this.query = new StringBuilder(initQuery);
        }

        public QueryBuilder withPrice(String field) {
            if (parameterMap.containsKey("priceMin") || parameterMap.containsKey("priceMax")) {
                double priceMin = 0.0;
                double priceMax = Double.MAX_VALUE;

                String priceMinStr = parameterMap.get("priceMin")[0];
                if (parameterMap.containsKey("priceMin") && ValidateXSS.validateFloatWithRegex(priceMinStr)) {
                    priceMin = Double.parseDouble(priceMinStr);
                }

                String priceMaxStr = parameterMap.get("priceMax")[0];
                if (parameterMap.containsKey("priceMax") && ValidateXSS.validateFloatWithRegex(priceMaxStr)) {
                    priceMax = Double.parseDouble(priceMaxStr);
                }

                query.append(" AND " + field + " BETWEEN '" + priceMin + "' AND '" + priceMax + "'");
            }

            return this;
        }

        public QueryBuilder withBrand(String field) {
            if (parameterMap.containsKey("brand")) {
                query.append(" AND " + field + " = '" + parameterMap.get("brand")[0] + "'");
            }

            return this;
        }

        public QueryBuilder withSortingBy(String defaultField, Map<String, String> sortingMap) {
            String field = defaultField;
            if (parameterMap.containsKey("sortBy")) {
                field = parameterMap.get("sortBy")[0];

                if (sortingMap.containsKey(field))
                    field = sortingMap.get(field);
            }

            query.append(" ORDER BY " + field);

            return this;
        }

        public QueryBuilder withPagination() {
            long recordsPerPage = DEFAULT_RECORDS_PER_PAGE;
            long page = 1L;

            if (parameterMap.containsKey("page")) {
                if (parameterMap.get("page") != null) {
                    String pageStr = parameterMap.get("page")[0];
                    if (ValidateXSS.validateStringIntegersWithRegex(pageStr))
                        page = Long.parseLong(pageStr);

                }

                if (parameterMap.get("recordsPerPage") != null) {
                    String recordsPerPageStr = parameterMap.get("recordsPerPage")[0];
                    if (ValidateXSS.validateStringIntegersWithRegex(recordsPerPageStr))
                        recordsPerPage = Long.parseLong(recordsPerPageStr);
                }
            }

            query.append(" LIMIT " + recordsPerPage);
            if (page > 1)
                query.append(" OFFSET " + recordsPerPage * (page - 1));

            return this;
        }

        public QueryBuilder withProductOptionValue(String field) {
            if (parameterMap.containsKey("option")) {
                String optionName = parameterMap.get("option")[0];
                String escapedOptionName = escapeSpecialSymbols(optionName);

                query.append(" AND " + field + " = '" + escapedOptionName + "'");
            }

            return this;
        }

        public QueryBuilder withLanguage(String field, String lang) {
            int id = Language.getIdFromLanguage(lang);
            query.append(" AND " + field + " = " + id);

            return this;
        }

        public QueryBuilder withCategory(String field) {
            if (parameterMap.containsKey("productCategory")) {
                long id = Long.parseLong(parameterMap.get("productCategory")[0]);
                query.append("WHERE " + field + " = " + id);
            }
            return this;
        }

        public QueryBuilder withGroupBy(String field) {
            query.append(" GROUP BY " + field);

            return this;
        }

        private String escapeSpecialSymbols(String str) {
            return str.replaceAll("'", "\\\\'")
                    .replaceAll("\"", "\\\\\"");
        }

        public String build() {
            return query.toString();
        }
    }
}
