package com.example.webappstore.dao.impl;

import com.example.webappstore.dao.exception.DAOException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class AbstractDAO {
    private DataSource dataSource;


    public AbstractDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    protected synchronized Connection getConnection() throws DAOException {
        return getConnection(true);
    }

        protected synchronized Connection getConnection(boolean autoCommit) throws DAOException {
        try {
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(autoCommit);
            return connection;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected void rollback(Connection connection) throws DAOException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected synchronized void closeConnection(Connection con) throws DAOException {
        try {
            con.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


}