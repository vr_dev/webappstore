package com.example.webappstore.dao.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.OrderDao;
import com.example.webappstore.entity.Order;
import com.example.webappstore.entity.OrderItem;
import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.Status;
import com.example.webappstore.service.factory.OrderFactory;
import com.example.webappstore.service.factory.OrderItemFactory;
import com.example.webappstore.service.factory.StatusFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.*;

public class OrderDaoImpl extends AbstractDAO implements OrderDao {

    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());
    private static final String SELECT_LAST_ID_FROM_ORDER = "SELECT MAX(id) FROM `order`";
    private static final String INSERT_INTO_ORDER = "INSERT `order`(create_time, status_id, user_id) VALUES(?,?,?)";
    private static final String SELECT_ALL_FROM_ORDER = "SELECT * FROM `order`";
    private static final String SELECT_FROM_4_TABLES =
            "SELECT `order`.id AS order_id,\n" +
                    "       `order`.create_time,\n" +
                    "       user.email AS user_email,\n" +
                    "       status.name\n" +
                    "FROM `order`\n" +
                    "    JOIN status ON `order`.status_id = status.id\n" +
                    "         JOIN order_content ON `order`.id = order_content.order_id\n" +
                    "         JOIN user ON user.id = `order`.user_id\n" +
                    "GROUP BY `order`.id";

    private static final String UPDATE_STATUS_ORDER = "UPDATE `order` SET status_id = ? WHERE id = ?";

    private static final String SELECT_ORDERS_BY_USER_ID = "SELECT `order`.id AS order_id,\n" +
            "       `order`.create_time,\n" +
            "       status.name\n" +
            "FROM `order`\n" +
            "JOIN status ON `order`.status_id = status.id\n" +
            "JOIN order_content ON `order`.id = order_content.order_id\n" +
            "JOIN user ON user.id = `order`.user_id\n" +
            "WHERE user.id= ? GROUP BY `order`.id";
    private static final String SELECT_ORDER_BY_ID = "SELECT * FROM `order` WHERE id = ?";
    private static final String INSERT_INTO_ORDER_ITEM =
            "INSERT order_content(order_id, product_id, quantity, price) VALUES(?,?,?,?)";
    private static final String DELETE_ORDER_BY_ID = "DELETE FROM `order` WHERE id = ?";
    private static final String DELETE_ORDER_ITEMS = "DELETE FROM order_content WHERE order_id = ?";
    private static final String SELECT_PRODUCT_ID = "SELECT product_id FROM order_content WHERE order_id = ?";


    public OrderDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public void createOrder(long userId, List<Product> productList) throws DAOException {
        Connection con = null;
        StatusFactory statusFactory = StatusFactory.getInstance();
        OrderFactory orderFactory = OrderFactory.getInstance();
        OrderItemFactory orderItemFactory = OrderItemFactory.getInstance();

        try {
            con = getConnection(false);

            Status status = statusFactory.fillStatus();
            Order order = orderFactory.fillOrder(status, userId);

            try (PreparedStatement pstmt = con.prepareStatement(INSERT_INTO_ORDER, Statement.RETURN_GENERATED_KEYS)) {
                pstmt.setTimestamp(1, order.getDateTime());
                pstmt.setLong(2, order.getStatusId());
                pstmt.setLong(3, order.getUserId());

                if (pstmt.executeUpdate() == 1) {
                    ResultSet gk = pstmt.getGeneratedKeys();
                    while (gk.next()) {
                        order.setId(gk.getLong(1));
                    }
                } else {
                    LOGGER.error("Insert into order failed");
                    throw new SQLException("Insert into order failed");
                }
            }

            for (Product product : productList) {
                long productId = product.getId();
                double price = product.getPrice();

                OrderItem orderItem = orderItemFactory.fillOrderItem(productId, price, order);

                try (PreparedStatement pstmt = con.prepareStatement(INSERT_INTO_ORDER_ITEM)) {
                    pstmt.setLong(1, orderItem.getOrderId());
                    pstmt.setLong(2, orderItem.getProductId());
                    pstmt.setLong(3, orderItem.getQuantity());
                    pstmt.setDouble(4, orderItem.getPrice());

                    if (pstmt.executeUpdate() != 1) {
                        LOGGER.error("Insert into order_content failed");
                        throw new SQLException("Insert into order_content failed");
                    }
                }
            }

            con.commit();

        } catch (SQLException e) {
            rollback(con);
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    @Override
    public Long takeLastID() throws DAOException {
        long lastId = 0;
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_LAST_ID_FROM_ORDER)) {
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    lastId = rs.getLong("MAX(ID)");
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return lastId;
    }

    @Override
    public ArrayList<Order> getAllOrder() throws DAOException {
        ArrayList<Order> orders = new ArrayList<>();
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_ALL_FROM_ORDER)) {
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    Order order = new Order();
                    order.setId(rs.getLong(ID));
                    order.setDateTime(rs.getTimestamp(DATE_TIME));
                    order.setStatusId(rs.getLong(STATUS));
                    orders.add(order);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return orders;
    }

    @Override
    public ArrayList<ArrayList<String>> getFrom4Tables() throws DAOException {
        ArrayList<ArrayList<String>> orders = new ArrayList<>();
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_FROM_4_TABLES)) {
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    ArrayList<String> order = new ArrayList<>();
                    order.add(String.valueOf(rs.getLong(ORDER_ID_TABLE)));
                    order.add(String.valueOf(rs.getTimestamp(CREATE_TIME)));
                    order.add(rs.getString(USER_EMAIL));
                    order.add(rs.getString(STATUS_NAME));
                    orders.add(order);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return orders;
    }

    @Override
    public void changeOrderStatus(Long orderId, Long statusId) throws DAOException {
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(UPDATE_STATUS_ORDER)) {

                pstmt.setLong(1, statusId);
                pstmt.setLong(2, orderId);

                int count = pstmt.executeUpdate();
                if (count != 1)
                    throw new SQLException("Insert updated " + count + " rows");
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    @Override
    public ArrayList<ArrayList<String>> getOrderByUserId(Long userId) throws DAOException {
        ArrayList<ArrayList<String>> orders = new ArrayList<>();
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_ORDERS_BY_USER_ID)) {
                pstmt.setLong(1, userId);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    ArrayList<String> order = new ArrayList<>();
                    order.add(String.valueOf(rs.getLong(ORDER_ID_TABLE)));
                    order.add(String.valueOf(rs.getTimestamp(CREATE_TIME)));
                    order.add(rs.getString(NAME));
                    orders.add(order);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return orders;
    }

    @Override
    public void deleteOrderById(Long orderId) throws DAOException {
        Connection con = null;
        try {
            con = getConnection(false);

            try (PreparedStatement pstmt = con.prepareStatement(DELETE_ORDER_ITEMS)) {
                pstmt.setLong(1, orderId);
                if(pstmt.executeUpdate() == 0)
                    throw new SQLException("can't delete from order_content");
            }

            try (PreparedStatement pstmt = con.prepareStatement(DELETE_ORDER_BY_ID)) {
                pstmt.setLong(1, orderId);
                if(pstmt.executeUpdate() == 0)
                    throw new SQLException("can't delete from order");
            }
            con.commit();

        } catch (SQLException e) {
            rollback(con);
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    @Override
    public Order getOrderById(Long orderId) throws DAOException {
        Order order = new Order();
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_ORDER_BY_ID)) {
                pstmt.setLong(1, orderId);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {

                    order.setId(rs.getLong(ID));
                    order.setDateTime(rs.getTimestamp(CREATE_TIME));
                    order.setStatusId(rs.getLong("status_id"));

                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return order;
    }

    @Override
    public void createOrderItem(OrderItem orderItem) throws DAOException {
        Connection con = null;
        try{
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(INSERT_INTO_ORDER_ITEM)) {
                pstmt.setLong(1, orderItem.getOrderId());
                pstmt.setLong(2, orderItem.getProductId());
                pstmt.setLong(3, orderItem.getQuantity());
                pstmt.setDouble(4, orderItem.getPrice());

                int count = pstmt.executeUpdate();
                if (count != 1)
                    throw new SQLException("Insert updated " + count + " rows");
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
    }

    @Override
    public ArrayList<Long> getProductsIdFromOrderItem(Long orderId ) throws DAOException {
        ArrayList<Long> productsId =new ArrayList<>();
        Connection con = null;
        try {
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_PRODUCT_ID)) {
                pstmt.setLong(1, orderId);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    productsId.add(rs.getLong(PRODUCT_ID_TABLE));
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return productsId;
    }


}
