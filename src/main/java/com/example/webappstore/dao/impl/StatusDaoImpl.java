package com.example.webappstore.dao.impl;

import com.example.webappstore.dao.exception.DAOException;
import com.example.webappstore.dao.interfaces.StatusDao;
import com.example.webappstore.entity.Status;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.*;

public class StatusDaoImpl extends AbstractDAO implements StatusDao {

    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());
    private static final String SELECT_ID_BY_STATUS_NAME = "SELECT id FROM status WHERE name = ? ";
    private static final String SELECT_STATUS_FROM_ORDER = "SELECT status.name, status.id FROM status " +
            "INNER JOIN `order` ON status.id = `order`.status_id WHERE `order`.id = ?";
    private static final String SELECT_ALL = "SELECT * FROM status";

    public StatusDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public Long getIdByStatusName(String statusName) throws DAOException {
        long id = 0;
        Connection con = null;
        try{
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_ID_BY_STATUS_NAME)) {
                pstmt.setString(1, statusName);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {

                    id = rs.getLong("id");
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }

        return id;
    }
    @Override
    public List<Status> getAllStatuses() throws DAOException {
        List<Status> statuses = new ArrayList<>();
        Connection con = null;
        try{
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_ALL)) {

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    Status status = new Status();
                    status.setId(rs.getLong(ID));
                    status.setStatusName(rs.getString(NAME));
                    statuses.add(status);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }
        return statuses;
    }
    @Override
    public Status getStatusByOrderId(Long orderId) throws DAOException {
        Status status = new Status();
        Connection con = null;
        try{
            con = getConnection();
            try (PreparedStatement pstmt = con.prepareStatement(SELECT_STATUS_FROM_ORDER)) {
                pstmt.setLong(1, orderId);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {

                    status.setId(rs.getLong(ID));
                    status.setStatusName(rs.getString(NAME));

                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            closeConnection(con);
        }

        return status;
    }

}
