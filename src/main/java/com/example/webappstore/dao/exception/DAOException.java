package com.example.webappstore.dao.exception;

import java.sql.SQLException;

public class DAOException extends SQLException {

    public DAOException(String reason) {
        super(reason);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String reason, Throwable cause) {
        super(reason, cause);
    }
}
