package com.example.webappstore.dao.mapper;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantNames.THUMB_URL;

public class ProductRowMapper implements RowMapper<Product> {

    @Override
    public Product map(ResultSet resultSet) throws SQLException {
        Product product = new Product();
        product.setId(resultSet.getLong(PRODUCTID));
        product.setCategory(new ProductCategory(resultSet.getInt(CATEGORY_ID)));
        product.setName(resultSet.getString(NAME));
        product.setBrand(resultSet.getString(BRAND));
        product.setDescription(resultSet.getString(DESCRIPTION));
        product.setCreated(resultSet.getTimestamp(CREATE_TIME));
        product.setPrice(resultSet.getDouble(PRICE));
        product.setImageUrl(resultSet.getString(IMAGE_URL));
        product.setThumbUrl(resultSet.getString(THUMB_URL));

        return product;
    }
}
