package com.example.webappstore.dao.mapper;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantNames.DESCRIPTION;

public class ProductCategoryRowMapper implements RowMapper<ProductCategory> {
    @Override
    public ProductCategory map(ResultSet resultSet) throws SQLException {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setId(resultSet.getInt(ID));
        productCategory.setName(resultSet.getString(CATEGORY_NAME));
        productCategory.setThumbUrl(resultSet.getString(THUMB_URL));
        return productCategory;
    }
}
