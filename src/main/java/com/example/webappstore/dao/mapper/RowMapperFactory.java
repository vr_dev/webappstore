package com.example.webappstore.dao.mapper;

import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.ProductCategory;

public class RowMapperFactory {
    private final RowMapper<Product> productRowMapper = new ProductRowMapper();
    private final RowMapper<ProductCategory> productCategoryRowMapper = new ProductCategoryRowMapper();


    public static RowMapperFactory getInstance() {
        return Holder.INSTANCE;
    }

    public RowMapper<Product> getProductRowMapper() {
        return productRowMapper;
    }

    public RowMapper<ProductCategory> getProductCategoryRowMapper() {
        return productCategoryRowMapper;
    }

    private static class Holder {
        private static final RowMapperFactory INSTANCE = new RowMapperFactory();
    }
}
