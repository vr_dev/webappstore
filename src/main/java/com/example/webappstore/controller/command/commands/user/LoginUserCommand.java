package com.example.webappstore.controller.command.commands.user;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.User;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;
import static com.example.webappstore.util.constants.ConstantPageNames.LOGIN_JSP;
import static com.example.webappstore.validator.ValidateActiveUser.checkAccess;

public class LoginUserCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = (UserService) request.getServletContext().getAttribute("userService");
        HttpSession session = request.getSession();


        String login = request.getParameter(EMAIL);
        String password = request.getParameter(PASSWORD);

        ExecutionResult result = new ExecutionResult();

        if (login != null && password != null) {
            String securedPassword = DigestUtils.md5Hex(password);
            User user = null;
            try {
                user = userService.getUserByLoginPassword(login, securedPassword);
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            }

            if (user != null) {
                if (checkAccess(user)) {
                    session.setAttribute(USER, user);
                    session.setAttribute(ROLE, user.getRole().getId());
                    result.setUrl(HOME_SERVICE);
                    result.setDirection(HttpDispatch.REDIRECT);

                } else {
                    request.setAttribute(ERROR, ERROR_USER_BLOCKED);

                    result.setUrl(LOGIN_JSP);
                    result.setDirection(HttpDispatch.FORWARD);
                }
            } else {
                request.setAttribute(ERROR, ERROR_EMAIL_OR_PASSWORD);
                result.setUrl(LOGIN_JSP);
                result.setDirection(HttpDispatch.FORWARD);
            }

        } else {
            result.setUrl(LOGIN_JSP);
            result.setDirection(HttpDispatch.FORWARD);
        }

        return result;
    }
}
