package com.example.webappstore.controller.command.commands;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.type.Language;
import com.example.webappstore.util.constants.ConstantNames;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static com.example.webappstore.util.constants.ConstantNames.LANGUAGE_PARAMETER;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;


public class ChangeLanguageCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer requestURL = request.getRequestURL();

        ExecutionResult result = new ExecutionResult();

        String choosenLanguage = request.getParameter(LANGUAGE_PARAMETER);
        String resultLanguage = Language.getLocaleFromLanguage(choosenLanguage).getLanguage();
        request.getSession().setAttribute(ConstantNames.LANGUAGE, resultLanguage);

        Cookie languageCookie = new Cookie(ConstantNames.LANGUAGE, resultLanguage);
        languageCookie.setMaxAge(60 * 60 * 24 * 21);
        response.addCookie(languageCookie);

        result.setUrl(HOME_SERVICE);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;

    }
}
