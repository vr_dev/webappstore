package com.example.webappstore.controller.command.commands.user;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.dao.impl.UserDaoImpl;
import com.example.webappstore.dao.interfaces.UserDao;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;

import static com.example.webappstore.util.constants.ConstantNames.ALL_USER;
import static com.example.webappstore.util.constants.ConstantNames.USER_ID;

public class ActivateUserCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = (UserService) request.getServletContext().getAttribute("userService");

        long userId = Long.valueOf(request.getParameter(USER_ID));
        try {
            userService.ActivateUser(userId, false);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        ExecutionResult result = new ExecutionResult();
        result.setUrl(ALL_USER);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;
    }
}
