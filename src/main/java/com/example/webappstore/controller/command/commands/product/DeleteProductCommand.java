package com.example.webappstore.controller.command.commands.product;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static com.example.webappstore.util.constants.ConstantNames.PRODUCT_ID;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;

public class DeleteProductCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");

        long productId = Long.valueOf(request.getParameter(PRODUCT_ID));

        try {
            productService.deleteProduct(productId);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }


        ExecutionResult result = new ExecutionResult();
        result.setUrl(HOME_SERVICE);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;

    }
}
