package com.example.webappstore.controller.command.commands.product;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.type.Language;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.example.webappstore.util.constants.ConstantNames.PRODUCT_ID;
import static com.example.webappstore.util.constants.ConstantNames.PRODUCT_MAP;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;
import static com.example.webappstore.util.constants.ConstantPageNames.UPDATE_PRODUCT_JSP;


public class UpdateProductCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");
        ExecutionResult result = new ExecutionResult();

        long productId = Long.valueOf(request.getParameter(PRODUCT_ID));

        String productNameRus = request.getParameter("nameRus");
        String productNameEng = request.getParameter("nameEng");
        try {
            if (productNameRus != null && productNameEng != null) {
                productService.updateProduct(request);

                result.setUrl(HOME_SERVICE);
                result.setDirection(HttpDispatch.REDIRECT);
            } else {

                Product productRus = productService.getProductById(productId, Language.RUS.getLanguage());
                Product productEng = productService.getProductById(productId, Language.ENG.getLanguage());

                Map<String, Product> productMap = new LinkedHashMap<>();

                productMap.put(Language.RUS.getLanguage(), productRus);
                productMap.put(Language.ENG.getLanguage(), productEng);

                request.setAttribute(PRODUCT_MAP, productMap);

                result.setUrl(UPDATE_PRODUCT_JSP);
                result.setDirection(HttpDispatch.FORWARD);
            }
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        return result;
    }
}
