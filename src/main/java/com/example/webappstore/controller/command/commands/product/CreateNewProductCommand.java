package com.example.webappstore.controller.command.commands.product;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.CATEGORIES;
import static com.example.webappstore.util.constants.ConstantNames.LANGUAGE;
import static com.example.webappstore.util.constants.ConstantPageNames.CREATE_PRODUCT_JSP;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;


public class CreateNewProductCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");

        String nameRus = request.getParameter("nameRus");
        String nameEng = request.getParameter("nameEng");

        ExecutionResult result = new ExecutionResult();
        if (nameRus != null && nameEng != null) {
            try {
                productService.createProduct(request);
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            }
            result.setUrl(HOME_SERVICE);
            result.setDirection(HttpDispatch.REDIRECT);

        } else {
            List<ProductCategory> categories;
            try {
                String language = (String) request.getSession().getAttribute(LANGUAGE);
                categories = productService.getAllCategories(language);
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            }

            request.setAttribute(CATEGORIES, categories);

            result.setUrl(CREATE_PRODUCT_JSP);
            result.setDirection(HttpDispatch.FORWARD);

        }

        return result;
    }
}
