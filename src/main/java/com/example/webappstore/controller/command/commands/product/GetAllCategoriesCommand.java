package com.example.webappstore.controller.command.commands.product;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.ProductCategory;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_JSP;


public class GetAllCategoriesCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");
        String language = (String) request.getSession().getAttribute(LANGUAGE);

        List<ProductCategory> categories;

        try {
            categories = productService.getAllCategories(language);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        request.setAttribute(CATEGORIES, categories);

        ExecutionResult result = new ExecutionResult();
        result.setUrl(HOME_JSP);
        result.setDirection(HttpDispatch.FORWARD);

        return result;

    }
}
