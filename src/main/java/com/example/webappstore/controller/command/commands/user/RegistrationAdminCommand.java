package com.example.webappstore.controller.command.commands.user;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.User;
import com.example.webappstore.entity.type.UserRole;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.factory.UserFactory;
import com.example.webappstore.service.interfaces.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.*;
import static com.example.webappstore.validator.ValidateRegistration.validateMailWithRegex;
import static com.example.webappstore.validator.ValidateRegistration.validatePasswordWithRegex;

public class RegistrationAdminCommand implements ICommand {
    private UserFactory userFactory = UserFactory.getInstance();


    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ExecutionResult result = new ExecutionResult();
        UserService userService = (UserService) request.getServletContext().getAttribute("userService");

        if(request.getParameter(EMAIL) != null) {
            String email = request.getParameter(EMAIL) ;
            try {
                if(userService.isEmailExist(email)){

                    request.setAttribute(ERROR, ERROR_EMAIL_EXIST);
                    result.setUrl(REGISTRATION_JSP);
                    result.setDirection(HttpDispatch.FORWARD);
                }else if(!validateMailWithRegex(email)) {
                    request.setAttribute(ERROR, ERROR_EMAIL_FORMAT);
                    result.setUrl(REGISTRATION_JSP);
                    result.setDirection(HttpDispatch.FORWARD);
                }else if(!validatePasswordWithRegex(request.getParameter(PASSWORD))){
                    request.setAttribute(ERROR, ERROR_PASSWORD_FORMAT);
                    result.setUrl(REGISTRATION_JSP);
                    result.setDirection(HttpDispatch.FORWARD);
                }else{
                    //todo
                    User newUser = userFactory.fillUser(request, UserRole.ADMIN.getId());
                    userService.addUser(newUser);
                    newUser.setId(userService.getUserByLoginPassword(newUser.getEmail(), newUser.getPassword()).getId());
                    session.setAttribute(USER, newUser);
                    //todo
    //                session.setAttribute(ROLE, newUser.getRole().getId());
                    session.setAttribute(ROLE, newUser.getRole());
                    result.setUrl(HOME_SERVICE);
                    result.setDirection(HttpDispatch.REDIRECT);
                }
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            }

        }else{
//            result.setUrl(REGISTRATION_JSP);
            result.setUrl(REGISTRATION_ADMIN_JSP);
            result.setDirection(HttpDispatch.FORWARD);
        }


        return result;

    }
}
