package com.example.webappstore.controller.command.commands.product;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.CATALOG_JSP;


public class GetAllProductsCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");

        Map<String, String[]> parameterMap = request.getParameterMap();
        List<Product> products;
        long totalProducts;

        Map<String, List<String>> optionsMap;
        try {
            products = productService.getAllProductByOptionAndCategory(request);
            optionsMap = productService.getAllOptionsByProductCategory(request);
            totalProducts = productService.getTotalProductCountByOptions(request);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        request.setAttribute(PRODUCTS, products);
        request.setAttribute(OPTION_MAP, optionsMap);
        request.setAttribute(TOTAL_PRODUCTS_COUNT, totalProducts);

        ExecutionResult result = new ExecutionResult();
        if (request.getMethod().equals("GET")) {
            result.setUrl(CATALOG_JSP);
            result.setDirection(HttpDispatch.FORWARD);
        } else if (request.getMethod().equals("POST")) {
            result.setUrl("productCatalog" + constructParameters(parameterMap));
            result.setDirection(HttpDispatch.REDIRECT);
        }

        return result;

    }

    private String constructParameters(Map<String, String[]> map) {
        StringBuilder builder = new StringBuilder();
        if (!map.isEmpty()) {
            builder.append("?");
            map.forEach((k, strings) -> builder.append(k + "=" + strings[0] + "&"));
        }
        builder.substring(0, builder.lastIndexOf(builder.toString()));

        return builder.toString();
    }


}
