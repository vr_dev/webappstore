package com.example.webappstore.controller.command.commands.cart;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.CART_JSP;

public class GetAllProductFromCartCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ExecutionResult result = new ExecutionResult();

        List<Product> productsInCart = (List<Product>) session.getAttribute(CART_LIST);
        double sumOfPrice = 0.0;
        if (productsInCart != null) {
            sumOfPrice = productsInCart.stream().mapToDouble(Product::getPrice).sum();
        }

        request.setAttribute(PRODUCTS_IN_CART, productsInCart);
        request.setAttribute(SUM_OF_PRICE, sumOfPrice);

        result.setUrl(CART_JSP);
        result.setDirection(HttpDispatch.FORWARD);

        return result;

    }
}
