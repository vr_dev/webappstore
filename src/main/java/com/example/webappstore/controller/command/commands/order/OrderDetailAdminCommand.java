package com.example.webappstore.controller.command.commands.order;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import com.example.webappstore.entity.Status;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.OrderService;
import com.example.webappstore.service.interfaces.ProductService;
import com.example.webappstore.service.interfaces.StatusService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.ORDER_DETAIL_JSP;

public class OrderDetailAdminCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");
        OrderService orderService = (OrderService) request.getServletContext().getAttribute("orderService");
        StatusService statusService = (StatusService) request.getServletContext().getAttribute("statusService");
        String language = request.getParameter(LANGUAGE_PARAMETER);

        ExecutionResult result = new ExecutionResult();

        long orderId = Long.parseLong(request.getParameter(ORDER_ID));
        ArrayList<Long> productsIds = null;
        try {
            productsIds = orderService.getProductsIdFromOrderItem(orderId);

            ArrayList<Product> products = new ArrayList<>();
            for (Long productId : productsIds) {
                products.add(productService.getProductById(productId, language));
            }

            List<Status> statuses = statusService.getAllStatuses();
            Status currentStatus = statusService.getStatusByOrderId(orderId);

            request.setAttribute(PRODUCTS, products);
            request.setAttribute(STATUSES, statuses);
            request.setAttribute(CURRENT_STATUS, currentStatus);
            request.setAttribute(ORDER_ID, orderId);


            result.setUrl(ORDER_DETAIL_JSP);
            result.setDirection(HttpDispatch.FORWARD);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        return result;

    }
}
