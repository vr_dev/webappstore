package com.example.webappstore.controller.command.commands.user;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.example.webappstore.util.constants.CommandStorage.HOME;
import static com.example.webappstore.util.constants.ConstantNames.USER;
import static com.example.webappstore.util.constants.ConstantPageNames.*;

public class LogOutUserCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.setAttribute(USER, null);

        ExecutionResult result = new ExecutionResult();
        result.setUrl(HOME_SERVICE);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;
    }
}
