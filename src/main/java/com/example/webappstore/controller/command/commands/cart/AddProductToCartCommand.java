package com.example.webappstore.controller.command.commands.cart;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import com.example.webappstore.util.constants.ConstantNames;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.PRODUCT_ID;
import static com.example.webappstore.util.constants.ConstantPageNames.CART_SERVICE;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;

public class AddProductToCartCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService productService = (ProductService) request.getServletContext().getAttribute("productService");

        ExecutionResult result = new ExecutionResult();

        HttpSession session = request.getSession();

        long productId = Long.parseLong(request.getParameter(PRODUCT_ID));
        String lang = (String) session.getAttribute(ConstantNames.LANGUAGE);

        try {
            Product product = productService.getProductById(productId, lang);

            List<Product> productsInCart = (List<Product>) session.getAttribute(ConstantNames.CART_LIST);
            productsInCart = checkIfCartListExistInSession(session, productsInCart);

            boolean alreadyInCart = false;
            for (Product cartProduct : productsInCart) {
                if (cartProduct.getId() == productId) {
                    alreadyInCart = true;
                    break;
                }
            }

            if (!alreadyInCart) {
                productsInCart.add(product);
                session.setAttribute(ConstantNames.CART_LIST, productsInCart);

                result.setUrl(CART_SERVICE);
                result.setDirection(HttpDispatch.REDIRECT);
            } else {
                result.setUrl(HOME_SERVICE);
                result.setDirection(HttpDispatch.REDIRECT);

            }
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    private List<Product> checkIfCartListExistInSession(HttpSession session, List<Product> productsInCart) {
        if(productsInCart == null) {
            productsInCart = new ArrayList<>();
            session.setAttribute(ConstantNames.CART_LIST, productsInCart);
        }
        return productsInCart;
    }
}
