package com.example.webappstore.controller.command.commands.order;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.OrderService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static com.example.webappstore.util.constants.ConstantNames.ORDER_ID;
import static com.example.webappstore.util.constants.ConstantPageNames.ORDERS_ADMIN_SERVICE;


public class DeleteOrderAdminCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        OrderService orderService = (OrderService) request.getServletContext().getAttribute("orderService");

        long orderId = Long.parseLong(request.getParameter(ORDER_ID));

        try {
            orderService.deleteOrderById(orderId);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        ExecutionResult result = new ExecutionResult();
        result.setUrl(ORDERS_ADMIN_SERVICE);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;

    }
}
