package com.example.webappstore.controller.command.commands.order;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.*;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.OrderService;
import com.example.webappstore.util.constants.ConstantNames;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.USER;
import static com.example.webappstore.util.constants.ConstantPageNames.HOME_SERVICE;

public class CreateOrderFromCartProductsCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        OrderService orderService = (OrderService) request.getServletContext().getAttribute("orderService");

        try {
            HttpSession session = request.getSession();
            long userId = ((User) session.getAttribute(USER)).getId();

            List<Product> productsInCart = (List<Product>) session.getAttribute(ConstantNames.CART_LIST);

            orderService.createOrder(userId, productsInCart);

            session.setAttribute(ConstantNames.CART_LIST, null);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        ExecutionResult result = new ExecutionResult();
        result.setUrl(HOME_SERVICE);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;
    }
}
