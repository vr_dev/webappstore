package com.example.webappstore.controller.command;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CommandMissing implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest req, HttpServletResponse resp) {
        ExecutionResult result = new ExecutionResult();
        result.setUrl("home");
        result.setDirection(HttpDispatch.REDIRECT);
        return result;
    }
}
