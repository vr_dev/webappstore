package com.example.webappstore.controller.command.commands.order;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.User;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.OrderService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.util.ArrayList;

import static com.example.webappstore.util.constants.ConstantNames.ORDERS;
import static com.example.webappstore.util.constants.ConstantNames.USER;
import static com.example.webappstore.util.constants.ConstantPageNames.*;


public class ListOfOrdersForUsersCommand implements ICommand {

    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
//        UserService userService = (UserService) request.getServletContext().getAttribute("userService");
        OrderService orderService = (OrderService) request.getServletContext().getAttribute("orderService");

        HttpSession session = request.getSession();

        ExecutionResult result = new ExecutionResult();

        User currentUser = (User)session.getAttribute(USER);
        if(!currentUser.isAdmin()){

            ArrayList<ArrayList<String>> orders = null;
            try {
                orders = orderService.getOrderByUserId(currentUser.getId());
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            }
            request.setAttribute(ORDERS, orders);


            result.setUrl(ORDERS_USERS_JSP);
            result.setDirection(HttpDispatch.FORWARD);
        }else {
            result.setUrl(LOGIN_SERVICE);
            result.setDirection(HttpDispatch.REDIRECT);
        }

        return result;

    }
}
