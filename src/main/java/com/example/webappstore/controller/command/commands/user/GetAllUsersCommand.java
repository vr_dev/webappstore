package com.example.webappstore.controller.command.commands.user;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.dao.impl.UserDaoImpl;
import com.example.webappstore.dao.interfaces.UserDao;
import com.example.webappstore.entity.User;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import com.example.webappstore.service.interfaces.UserService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.USER;
import static com.example.webappstore.util.constants.ConstantNames.USERS;
import static com.example.webappstore.util.constants.ConstantPageNames.*;


public class GetAllUsersCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        UserService userService = (UserService) request.getServletContext().getAttribute("userService");

        ExecutionResult result = new ExecutionResult();

        User currentUser = (User)session.getAttribute(USER);
        if(currentUser.isAdmin()){
            List<User> users = null;
            try {
                users = userService.getUsers();
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            }
            request.setAttribute(USERS, users);

            result.setUrl(ALL_USERS_JSP);
            result.setDirection(HttpDispatch.FORWARD);

        }else {
            result.setUrl(LOGIN_SERVICE);
            result.setDirection(HttpDispatch.REDIRECT);
        }

        return result;
    }
}
