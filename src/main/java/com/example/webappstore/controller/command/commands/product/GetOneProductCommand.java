package com.example.webappstore.controller.command.commands.product;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import com.example.webappstore.service.exception.ServiceException;
import com.example.webappstore.service.interfaces.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static com.example.webappstore.util.constants.ConstantNames.*;
import static com.example.webappstore.util.constants.ConstantPageNames.PRODUCT_DETAIL_JSP;

public class GetOneProductCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        ProductService orderService = (ProductService) request.getServletContext().getAttribute("productService");
        String language = (String) request.getSession().getAttribute(LANGUAGE);

        long productId = Long.valueOf(request.getParameter(ID));
        Product product = null;
        try {
            product = orderService.getProductById(productId, language);
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }

        request.setAttribute(PRODUCT, product);
        ExecutionResult result = new ExecutionResult();
        result.setUrl(PRODUCT_DETAIL_JSP);
        result.setDirection(HttpDispatch.FORWARD);

        return result;
    }
}
