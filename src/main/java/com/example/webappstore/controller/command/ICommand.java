package com.example.webappstore.controller.command;

import com.example.webappstore.controller.ExecutionResult;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface ICommand {
    ExecutionResult execute(HttpServletRequest req, HttpServletResponse resp);
}
