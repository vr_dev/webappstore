package com.example.webappstore.controller.command.commands.cart;

import com.example.webappstore.controller.ExecutionResult;
import com.example.webappstore.controller.HttpDispatch;
import com.example.webappstore.controller.command.ICommand;
import com.example.webappstore.entity.Product;
import com.example.webappstore.util.constants.ConstantNames;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.util.List;

import static com.example.webappstore.util.constants.ConstantNames.PRODUCT_ID;
import static com.example.webappstore.util.constants.ConstantPageNames.CART_SERVICE;

public class DeleteProductFromCartCommand implements ICommand {
    @Override
    public ExecutionResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        long productId = Long.parseLong(request.getParameter(PRODUCT_ID));

        List<Product> productsInCart = (List<Product>) session.getAttribute(ConstantNames.CART_LIST);
        if (productsInCart != null ) {
            for (Product cartProduct : productsInCart) {
                if (cartProduct.getId() == productId) {
                    productsInCart.remove(cartProduct);
                    break;
                }
            }
        }

        ExecutionResult result = new ExecutionResult();
        result.setUrl(CART_SERVICE);
        result.setDirection(HttpDispatch.REDIRECT);

        return result;


    }
}
