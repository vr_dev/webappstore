package com.example.webappstore.controller;

public class ExecutionResult {
    private String url;

    private HttpDispatch direction;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public HttpDispatch getDirection() {
        return direction;
    }

    public void setDirection(HttpDispatch direction) {
        this.direction = direction;
    }
}
