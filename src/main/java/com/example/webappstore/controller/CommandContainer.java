package com.example.webappstore.controller;

import com.example.webappstore.controller.command.CommandMissing;
import com.example.webappstore.controller.command.ICommand;
import jakarta.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

public class CommandContainer {
    private static CommandContainer instance = new CommandContainer();
    Map<String, ICommand> commands = new HashMap<>();

    private CommandContainer() {
    }

    public ICommand getCommand(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        ICommand command = commands.get(servletPath);

        if (command == null)
            command = new CommandMissing();

        return command;
    }

    public void addCommand(String name, ICommand command) {
        commands.put(name, command);
    }

    public static CommandContainer getInstance() {
        if (instance == null)
            instance = new CommandContainer();
        return instance;
    }

}
