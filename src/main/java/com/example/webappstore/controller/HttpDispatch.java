package com.example.webappstore.controller;

public enum HttpDispatch {
    FORWARD,
    REDIRECT
}
