package com.example.webappstore.controller;

import com.example.webappstore.controller.command.ICommand;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public class Controller extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        ICommand command = CommandResolver.getInstance().getCommand(request);
        //todo: change resolver and container logic
        CommandContainer commandContainer = (CommandContainer) request.getServletContext().getAttribute("commandContainer");
        ICommand command = commandContainer.getCommand(request);
        ExecutionResult result = command.execute(request, response);


//        String url = result.getUrl();

        if (result.getDirection() == HttpDispatch.FORWARD)
            request.getRequestDispatcher(result.getUrl()).forward(request, response);
        if (result.getDirection() == HttpDispatch.REDIRECT) {
            response.sendRedirect(composeRedirectURL(request, result));
        }
    }

    private String composeRedirectURL(HttpServletRequest request, ExecutionResult result) {
        String url = request.getRequestURI().toString();
        url = url.substring(0, url.lastIndexOf('/') + 1);
        String location = url + result.getUrl();
        return location;
    }
}
