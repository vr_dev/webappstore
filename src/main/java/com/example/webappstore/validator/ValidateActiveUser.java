package com.example.webappstore.validator;

import com.example.webappstore.entity.User;

public class ValidateActiveUser {

    public static boolean checkAccess(User user) {
        return !user.isBlocked();
    }
}
