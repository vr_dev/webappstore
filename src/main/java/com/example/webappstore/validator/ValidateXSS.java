package com.example.webappstore.validator;

public class ValidateXSS {
        private static final String DIGITS_REGEX = "^(\\d*)$";
        private static final String FLOAT_REGEX = "^([\\d]+[.]*[\\d]+)$";
        public static boolean validateStringIntegersWithRegex(String number) {
            return number.matches(DIGITS_REGEX);
        }
        public static boolean validateFloatWithRegex(String number) {
            return number.matches(FLOAT_REGEX);
        }
}
