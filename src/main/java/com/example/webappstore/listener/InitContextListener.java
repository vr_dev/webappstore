package com.example.webappstore.listener;

import com.example.webappstore.controller.CommandContainer;
import com.example.webappstore.controller.command.CommandMissing;
import com.example.webappstore.controller.command.commands.*;
import com.example.webappstore.controller.command.commands.cart.AddProductToCartCommand;
import com.example.webappstore.controller.command.commands.cart.DeleteProductFromCartCommand;
import com.example.webappstore.controller.command.commands.cart.GetAllProductFromCartCommand;
import com.example.webappstore.controller.command.commands.order.*;
import com.example.webappstore.controller.command.commands.product.*;
import com.example.webappstore.controller.command.commands.user.*;
import com.example.webappstore.dao.impl.*;
import com.example.webappstore.service.impl.*;
import com.example.webappstore.service.interfaces.*;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import static com.example.webappstore.util.constants.CommandStorage.*;

public class InitContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        initDatasource(context);
        initServices(context);
    }

    private void initDatasource(ServletContext context) throws IllegalStateException {
        String dataSourceName = context.getInitParameter("dataSource");
        Context jndiContext = null;
        try {
            jndiContext = (Context) new InitialContext().lookup("java:/comp/env");
            DataSource dataSource = (DataSource) jndiContext.lookup(dataSourceName);
            context.setAttribute("dataSource", dataSource);
        } catch (NamingException e) {
            throw new IllegalStateException("Cannot initialize dataSource", e);
        }
    }


    private void initServices(ServletContext context) {
        DataSource dataSource = (DataSource) context.getAttribute("dataSource");

        ProductService productService = new ProductServiceImpl(new ProductDaoImpl(dataSource));
        OrderService orderService = new OrderServiceImpl(new OrderDaoImpl(dataSource));
        StatusService statusService = new StatusServiceImpl(new StatusDaoImpl(dataSource));
        UserService userService= new UserServiceImpl(new UserDaoImpl(dataSource));

        context.setAttribute("productService", productService);
        context.setAttribute("orderService", orderService);
        context.setAttribute("statusService", statusService);
        context.setAttribute("userService", userService);

        initCommands(context);
    }

    private void initCommands(ServletContext context) {
        CommandContainer commands = CommandContainer.getInstance();
/*
        commands.addCommand(ROOT, new CommandMissing());
        commands.addCommand(ROOT_2, new CommandMissing());

        commands.addCommand("/home", new GetAllCategoriesCommand());
        commands.addCommand("/productCatalog", new GetAllProductsCommand());
        commands.addCommand("/registration", new RegistrationUserCommand());
        commands.addCommand("/registrationAdmin", new RegistrationAdminCommand());
        commands.addCommand("/login", new LoginUserCommand());
        commands.addCommand("/product", new GetOneProductCommand());
        commands.addCommand("/addProductToCart", new AddProductToCartCommand());
        commands.addCommand("/cart", new GetAllProductFromCartCommand());
        commands.addCommand("/deleteProductFromCart", new DeleteProductFromCartCommand());
        commands.addCommand("/createProduct", new CreateNewProductCommand());
        commands.addCommand("/updateProduct", new UpdateProductCommand());
        commands.addCommand("/deleteProduct", new DeleteProductCommand());
        commands.addCommand("/allUsers", new GetAllUsersCommand());
        commands.addCommand("/activateUser", new ActivateUserCommand());
        commands.addCommand("/deactivateUser", new DeactivateUserCommand());
        commands.addCommand("/logout", new LogOutUserCommand());
        commands.addCommand("/createOrder", new CreateOrderFromCartProductsCommand());
        commands.addCommand("/ordersAdmin", new ListOfOrdersForAdminCommand());
        commands.addCommand("/ordersUser", new ListOfOrdersForUsersCommand());
        commands.addCommand("/orderDetail", new OrderDetailAdminCommand());
        commands.addCommand("/changeStatus", new ChangeStatusCommand());
        commands.addCommand("/DeleteOrderAdminService", new DeleteOrderAdminCommand());
        commands.addCommand("/changeLanguage", new ChangeLanguageCommand());
        commands.addCommand("/order", new OrderCommand());
*/
        commands.addCommand(ROOT, new CommandMissing());
        commands.addCommand(ROOT_2, new CommandMissing());

        commands.addCommand(HOME, new GetAllCategoriesCommand());
        commands.addCommand(PRODUCT_CATALOG, new GetAllProductsCommand());
        commands.addCommand(REGISTRATION, new RegistrationUserCommand());
        commands.addCommand(REGISTRATION_ADMIN, new RegistrationAdminCommand());
        commands.addCommand(LOGIN, new LoginUserCommand());
        commands.addCommand(PRODUCT, new GetOneProductCommand());
        commands.addCommand(ADD_PRODUCT_TO_CART, new AddProductToCartCommand());
        commands.addCommand(CART, new GetAllProductFromCartCommand());
        commands.addCommand(DELETE_PRODUCT_FROM_CART, new DeleteProductFromCartCommand());
        commands.addCommand(CREATE_PRODUCT, new CreateNewProductCommand());
        commands.addCommand(UPDATE_PRODUCT, new UpdateProductCommand());
        commands.addCommand(DELETE_PRODUCT, new DeleteProductCommand());
        commands.addCommand(ALL_USERS, new GetAllUsersCommand());
        commands.addCommand(ACTIVATE_USER, new ActivateUserCommand());
        commands.addCommand(DEACTIVATE_USER, new DeactivateUserCommand());
        commands.addCommand(LOGOUT, new LogOutUserCommand());
        commands.addCommand(CREATE_ORDER, new CreateOrderFromCartProductsCommand());
        commands.addCommand(ORDERS_ADMIN, new ListOfOrdersForAdminCommand());
        commands.addCommand(ORDERS_USER, new ListOfOrdersForUsersCommand());
        commands.addCommand(ORDER_DETAIL, new OrderDetailAdminCommand());
        commands.addCommand(CHANGE_STATUS, new ChangeStatusCommand());
        commands.addCommand(DELETE_ORDER_ADMIN_SERVICE, new DeleteOrderAdminCommand());
        commands.addCommand(CHANGE_LANGUAGE, new ChangeLanguageCommand());
        commands.addCommand(ORDER, new OrderCommand());

        context.setAttribute("commandContainer", commands);
    }
}
