<%@ include file="header.jsp" %>

<div class="container">

    <form id="contact" action="createProduct" method="post">
        <h3><fmt:message key="button.createProduct"/></h3>

        <fieldset>
            <label></label>
            <select class="max_size" name="productCategory">
                <c:forEach var="category" items="${categories}">
                    <option value="<c:out value="${category.id}"/>">${category.name}</option>
                </c:forEach>
            </select>
        </fieldset>
        <br/>

        <fieldset>
            <label><fmt:message key="label.imageProduct"/></label>
            <input class="max_size" placeholder="<fmt:message key="label.imageProduct"/>" type="text" name="imageUrl"
                   required/>
        </fieldset>
        <br/>

        <fieldset>
            <label><fmt:message key="label.thumbProduct"/></label>
            <input class="max_size" placeholder="<fmt:message key="label.thumbProduct"/>" type="text" name="thumbUrl"
                   required/>
        </fieldset>
        <br/>

        <fieldset>
            <label><fmt:message key="label.price"/></label>
            <input class="max_size" placeholder="<fmt:message key="label.price"/>" type="number" name="price"
                   pattern="[\d]+[.]*[\d]+" required/>
        </fieldset>
        <br/>

        <div class="float_container">
            <div class="float_child">
                <fieldset>
                    <label><fmt:message key="label.product"/></label><br/>
                    <input class="max_size" placeholder="<fmt:message key="label.product"/>" name="nameRus" type="text"
                           required/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.brand"/></label>
                    <input class="max_size" placeholder="<fmt:message key="label.brand"/>" name="brandRus" type="text"
                           required/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.descriptionProduct"/></label>
                    <textarea style="width: 100%; height: 400px;"
                              placeholder="<fmt:message key="label.descriptionProduct"/>"
                              name="descriptionRus"></textarea>
                </fieldset>
                <br/>

                <div class="float_container">
                    <c:forEach var="i" begin="1" end="20">
                        <div class="float_child">
                            <input class="max_size" type="text" name="optionNameRus${i}"/>
                        </div>
                        <div class="float_child">
                            <input class="max_size" type="text" name="optionValueRus${i}"/>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <div class="float_child">
                <fieldset>
                    <label><fmt:message key="label.product"/></label><br/>
                    <input class="max_size" placeholder="<fmt:message key="label.product"/>" name="nameEng" type="text"
                           required autofocus/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.brand"/></label>
                    <input class="max_size" placeholder="<fmt:message key="label.brand"/>" name="brandEng" type="text"
                           required/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.descriptionProduct"/></label>
                    <textarea style="width: 100%; height: 400px;"
                              placeholder="<fmt:message key="label.descriptionProduct"/>"
                              name="descriptionEng"></textarea>
                </fieldset>
                <br/>

                <div class="float_container">
                    <c:forEach var="i" begin="1" end="20">
                        <div class="float_child">
                            <input class="max_size" type="text" name="optionNameEng${i}"/>
                        </div>
                        <div class="float_child">
                            <input class="max_size" type="text" name="optionValueEng${i}"/>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

        <br/>
        <br/>
            <button name="submit" type="submit"><fmt:message key="button.submit"/></button>
        <br/>

    </form>
</div>
