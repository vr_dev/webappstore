<%@ include file="header.jsp" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tlds/custom.tld" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<div class="carousel-item active">
    <div class="container ">
        <br/>
        <div class="row"><br/>
            <form action="productCatalog" id="options" method="get">
                <input type="hidden" name="productCategory" value="${paramValues['productCategory'][0]}">
                <select name="option" style="width: 500px">
                    <c:forEach var="entry" items="${optionsMap}">
                        <div style="display: inline">
                            <optgroup label="${entry.key}">
                                <c:forEach var="optionValue" items="${entry.value}">
                                    <option value="<c:out value="${optionValue}"/>">${optionValue}</option>
                                </c:forEach>
                            </optgroup>
                        </div>
                    </c:forEach>
                </select>
                <button type="submit">Submit</button>
            </form>
        </div>
        <br/>
        <div class="row">
            <form action="productCatalog" method="get">
                <input type="hidden" name="productCategory" value="${paramValues['productCategory'][0]}">
                <label>Price</label>
                <input name="priceMin" style="width: 100px" value="${paramValues['priceMin'][0]}" pattern="[\d]+[.]*[\d]+"/>
                <label>to</label>
                <input name="priceMax" style="width: 100px" value="${paramValues['priceMax'][0]}" pattern="[\d]+[.]*[\d]+"/>
                <button type="submit">Submit</button>
            </form>
        </div>
        <br/>
        <hr/>

        <div class="row">
            <form action="productCatalog" method="get">
                <c:forEach var="entry" items="${paramValues}">
                    <c:if test="${entry.key ne 'sortBy'}">
                        <input type="hidden" name="${entry.key}" value="${entry.value[0]}"/>
                    </c:if>
                </c:forEach>
                <label>Sort By</label>
                <select name="sortBy" style="width: 500px">
                    <option value="titleUp">
                        Name (A-Z)
                    </option>
                    <option value="titleDown">
                        Name (Z-A)
                    </option>
                    <option value="priceUp">
                        Price Up
                    </option>
                    <option value="priceDown">
                        Price Down
                    </option>
                    <option value="latest">
                        Latest
                    </option>
                </select>
                <button type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>

<!-- product section -->

<section class="product_section layout_padding">
    <center>
        <div class="align-content-center">
            <c:set var="page" value="${param.page eq null ? 1 : param.page}"/>

            <mytags:pagination currentPage="${page}" entitiesPerPage="9" entitiesTotal="${totalProductsCount}"/>
        </div>
    </center>

    <div class="row">
        <c:forEach var="product" items="${products }">
            <div class="col-sm-6 col-lg-4">
                <a href="product?id=${product.id}">
                    <div class="box">
                        <div class="detail-box">
                            <div class="img-box">
                                <img src="${product.getThumbUrl()}" alt="">
                            </div>
                            <h5>
                                    ${product.getName()}
                            </h5>
                        </div>
                        <div class="product_info">
                            <h5>
                                    ${product.getPrice()} <span><fmt:message key="label.priceTag"/></span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
        </c:forEach>
    </div>

    <br/>
    <center>
        <div class="align-content-center">
            <c:set var="page" value="${param.page eq null ? 1 : param.page}"/>
            <mytags:pagination currentPage="${page}" entitiesPerPage="9" entitiesTotal="${totalProductsCount}"/>
        </div>
    </center>
</section>

<!-- end product section -->


<%@ include file="footer.jsp" %>
