<%@ taglib prefix="ctg" uri="/WEB-INF/tlds/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Pagination" pageEncoding="UTF-8" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<%@ attribute name="currentPage" required="true" type="java.lang.Integer" %>
<%@ attribute name="entitiesPerPage" type="java.lang.Integer" required="true" %>
<%@ attribute name="entitiesTotal" type="java.lang.Integer" required="true" %>

<c:set var="totalPages" value="${Integer.valueOf(entitiesTotal / entitiesPerPage)}"/>
<c:if test="${totalPages eq 0}">
    <c:set var="totalPages" value="${Integer.valueOf(1)}"/>
</c:if>

<c:if test="${currentPage != 1}">
    <a style="display: inline"
       href="?page=${currentPage - 1}&<ctg:joinGetParameters ignoreParameter="page"/>"><fmt:message
            key="pagination.prev"/></a>
</c:if>

<c:forEach var="i" begin="1" end="${totalPages}">
    <c:choose>
        <c:when test="${currentPage == i}">
            <h1 style="display: inline">${i}</h1>
        </c:when>
        <c:otherwise>
            <a style="display: inline" href="?page=${i}&<ctg:joinGetParameters ignoreParameter="page"/>">
                <h1 style="display: inline">${i}</h1>
            </a>
        </c:otherwise>
    </c:choose>
</c:forEach>

<c:if test="${totalPages > currentPage}">
    <a style="display: inline"
       href="?page=${currentPage + 1}&<ctg:joinGetParameters ignoreParameter="page"/>"><fmt:message
            key="pagination.next"/></a>
</c:if>
