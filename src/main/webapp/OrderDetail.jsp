
<%@ include file="header.jsp" %>

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Images</th>
                            <th>Product Name</th>
                            <th>Price</th>


                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var = "product" items="${products }">
                        <tr>
                            <td class="thumbnail-img">
                                <a href="product?id=${product.getId() }">
                                    <img class="img-fluid" src="${product.thumbUrl}" alt="" />
                                </a>
                            </td>
                            <td class="name-pr">
                                <a href="product?id=${product.id}">
                                    ${product.name}
                                </a>
                            </td>
                            <td class="price-pr">
                                <p>${product.price} <fmt:message key="label.priceTag"/></p>
                            </td>


                        </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div style="text-align: center;">
            <h3><fmt:message key="label.currentStatus"/> - <strong>${currentStatus.getStatusName()}</strong>   </h3>

        </div>

        <c:if test="${sessionScope.user.isAdmin() == true}">
            <div style="margin-top: 100px;margin-bottom: 100px; text-align: center;">
            <form action="changeStatus" method="post">
                <p><select name="statusId">
                    <c:forEach var = "status" items="${statuses }">
                        <option  value="<c:out value="${status.getId()}"/>" /><c:out value="${status.getStatusName()}" /></option>
                    </c:forEach>
                </select>
                    <input type="submit" value="<fmt:message key="button.changeStatus"/>"></p>
                <input type="hidden" name="orderId" value="<c:out value="${orderId}"/>" />
            </form>

            <form action="DeleteOrderAdminService" method="post">
                <input type="hidden" name="orderId" value="<c:out value="${orderId}"/>" />
                <button class="btn btn-danger btn-lg"><fmt:message key="button.deleteOrder"/></button>
            </form>

        </div>
        </c:if>
        <c:if test="${sessionScope.user.isAdmin() == false}">
            <div style="margin-top: 100px;margin-bottom: 100px; text-align: center;">


            <c:if test="${currentStatus.getId()==1 or currentStatus.getId()==2}" >

                <form action="changeStatus" method="post">
                    <input type="hidden" name="statusId" value="3" />
                    <input type="hidden" name="orderId" value="<c:out value="${orderId}"/>" />
                    <button class="btn btn-danger btn-lg"><fmt:message key="button.cancelOrder"/></button>
                </form>

            </c:if>
            </div>
        </c:if>

    </div>
</div>
<!-- End Cart -->

</body>

</html>