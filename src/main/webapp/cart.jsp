



<%@ include file="header.jsp" %>
<div class="page-content-product">
    <div class="main-product">
        <div class="container">

            <div class="row clearfix">
                <div class="container">
                    <div class="find-box">


                    </div>
                </div>
                <div class="find-box">
                    <h1><fmt:message key="label.cart.productInMyCart"/></h1>
                    <h4><fmt:message key="label.cart.sumOfProducts"/> <c:out value="${sumOfPrice}"/> <span><fmt:message key="label.priceTag"/></span></h4>
                    <c:if test="${sumOfPrice != 0.0 and sessionScope.user != null}">
                        <form action="createOrder" method="post" style="text-align: center">
                            <button  class="btn btn-success lg"><fmt:message key="button.orderProducts"/></button>
                        </form>
                    </c:if>

                </div>
            </div>

            <div class="row clearfix">
                <c:forEach var = "product" items="${productsInCart }">
                <div class="col-lg-3 col-sm-6 col-md-3">
                    <a href="product?id=${product.id }">
                        <div class="box-img">
                            <h4>${product.name}</h4>
                            <img src="${product.thumbUrl}" alt="" />
                            <h2>${product.price}</h2>

                            <form action="deleteProductFromCart" method="post">
                                <input type="hidden" name="productId" value="<c:out value="${product.id}"/>" />
                                <button  class="btn btn-danger"><fmt:message key="button.deleteProductFromCart"/></button>
                            </form>
                        </div>
                    </a>
                </div>
                </c:forEach>

            </div>

        </div>
    </div>
</div>
<%@ include file="footer.jsp" %>

