<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!-- Site Metas -->
    <link rel="icon" href="images/fevicon.png" type="image/gif"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <title>EPAM</title>


    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <!-- fonts style -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
    <!-- range slider -->

    <!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!-- responsive style -->
    <link href="css/responsive.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<div class="hero_area">
    <!-- header section strats -->
    <header class="header_section">
        <div class="header_bottom">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg custom_nav-container ">
                    <a class="navbar-brand" href="home">
                        <span>EPAM</span>
                    </a>

                    <div class="user_option_box">
                        <form action="changeLanguage" method="get">
                            <c:if test="${sessionScope.lang == 'ru'}">
                                <input type="hidden" name="language" value="en">
                            </c:if>
                            <c:if test="${sessionScope.lang == 'en'}">
                                <input type="hidden" name="language" value="ru">
                            </c:if>
                            <div class="form-sh">
                                <input type="submit" class="btn btn-primary btn-sm" value="<fmt:message key="button.language"/>">
                            </div>
                        </form>

                        <div class="header_margin_section">
                            <c:choose>
                                <c:when test="${sessionScope.user eq null}">
                                    <%--<i class="fa fa-user" aria-hidden="true"></i>--%>
                                    <a href="login" class="account-link">
                                        <span><fmt:message key="button.login"/></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a href="logout" class="account-link">
                                        <span><fmt:message key="button.logout"/></span>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                            <a href="cart" class="cart-link">
                                <span><fmt:message key="button.myCart"/></span>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- end header section -->