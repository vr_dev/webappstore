<%@ include file="header.jsp" %>


<div class="product-page-main">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="prod-page-title">
                    <h2><c:out value="${product.getName()}"/></h2>

                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-7 col-sm-8">
                <div class="md-prod-page">
                    <div class="md-prod-page-in">
                        <div class="page-preview">
                            <div class="preview">
                                <div class="preview-pic tab-content">
                                    <div class="tab-pane active" id="pic-1"><img
                                            src="<c:out value="${product.getThumbUrl()}" />"
                                            alt="#"/></div>

                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="description-box">
                        <div class="dex-a">
                            <h4><fmt:message key="label.descriptionProduct"/></h4>
                            <p><c:out value="${product.description}"/></p>
                        </div>
                    </div>

                    <div class="description-box">
                        <div class="dex-a">
                            <p>
                            <table>
                                <c:set var="optionsMap" value="${product.options.attributes}"/>

                                <c:forEach var="entry" items="${optionsMap}">
                                    <tr>
                                        <td><c:out value="${entry.key}"/></td>
                                        <td><c:out value="${entry.value}"/></td>
                                    </tr>
                                </c:forEach>
                            </table>
                            </p>
                        </div>

                    </div>

                </div>


            </div>
            <div class="col-md-3 col-sm-12">
                <div class="price-box-right">
                    <c:if test="${sessionScope.user.isAdmin() == true}">
                        <form action="updateProduct" method="post">

                            <input type="hidden" name="productId" value="<c:out value="${product.getId()}"/>"/>
                            <button class="btn btn-danger btn-lg"><fmt:message key="button.updateProduct"/></button>
                        </form>
                        <br>
                        <form action="deleteProduct" method="post">

                            <input type="hidden" name="productId" value="<c:out value="${product.getId()}"/>"/>
                            <button class="btn btn-danger btn-lg"><fmt:message key="button.deleteProduct"/></button>
                        </form>
                    </c:if>
                    <br>
                    <h4><fmt:message key="label.price"/></h4>
                    <h3><c:out value="${product.getPrice()}"/> <span><fmt:message key="label.priceTag"/></span></h3>

                    <form action="addProductToCart" method="post">
                        <input type="hidden" name="productId" value="<c:out value="${product.getId()}"/>"/>
                        <button class=""><fmt:message key="button.addToCart"/></button>

                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

