<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- footer section -->
<footer class="footer_section">
    <div class="container">
        <p>
            &copy; <span <%--id="displayYear"--%>>2022</span> EPAM educational project</a>
        </p>
    </div>
</footer>
<!-- footer section -->

<!-- jQery -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>


</body>

</html>