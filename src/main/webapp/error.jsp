<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>EPAM</title>
    <%--<link type="text/css" rel="stylesheet" href="css/style.css"/>--%>
</head>
<body>
<h1><fmt:message var="errorPage.oops"/></h1>
<h2><fmt:message var="errorPage.title"/></h2>
<p><fmt:message var="errorPage.contactUs"/></p>

<a href="home"><fmt:message var="errorPage.toHome"/></a>
</body>
</html>

