<%@ include file="header.jsp" %>

<div class="container">

    <form id="contact" action="updateProduct" method="post">
        <c:set var="productRus" value="${productMap.ru}"/>
        <c:set var="productEng" value="${productMap.en}"/>

        <h3><fmt:message key="button.updateProduct"/></h3>
        <input type="hidden" name="productId" value="<c:out value="${productRus.getId()}"/>"/>
        <input type="hidden" name="productCategory" value="<c:out value="${productRus.category.id}"/>"/>

        <fieldset>
            <label><fmt:message key="label.imageProduct"/></label>
            <input class="max_size" placeholder="<fmt:message key="label.imageProduct"/>" type="text"
                   value="<c:out value="${productRus.imageUrl}"/>" tabindex="5" name="imageUrl" required/>
        </fieldset>
        <br/>

        <fieldset>
            <label><fmt:message key="label.imageProduct"/></label>
            <input class="max_size" placeholder="<fmt:message key="label.imageProduct"/>" type="text"
                   value="<c:out value="${productRus.thumbUrl}"/>" tabindex="6" name="thumbUrl" required/>
        </fieldset>
        <br/>

        <fieldset>
            <label><fmt:message key="label.price"/></label>
            <input class="max_size" placeholder="<fmt:message key="label.price"/>" type="number" tabindex="7"
                   value="<c:out value="${productRus.price}"/>" name="price" pattern="[\d]+[.]*[\d]+" required/>
        </fieldset>
        <br/>

        <div class="float_container">
            <div class="float_child">
                <fieldset>
                    <label><fmt:message key="label.product"/></label><br/>
                    <input class="max_size" placeholder="<fmt:message key="label.product"/>" name="nameRus" type="text"
                           value="<c:out value="${productRus.name}"/>" tabindex="1" required autofocus/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.brand"/></label>
                    <input class="max_size" placeholder="<fmt:message key="label.brand"/>" name="brandRus" type="text"
                           value="<c:out value="${productRus.brand}"/>" tabindex="2" required autofocus/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.descriptionProduct"/></label>
                    <textarea style="width: 100%; height: 400px;"
                              placeholder="<fmt:message key="label.descriptionProduct"/>" tabindex="4"
                              name="descriptionRus"
                              required><c:out value="${productRus.description}"/></textarea>
                </fieldset>
                <br/>


                <c:set var="optionsMap" value="${productRus.options.attributes}"/>
                <div class="float_container">
                    <c:forEach var="entry" items="${optionsMap}" varStatus="counter">
                        <div class="float_child">
                            <input class="max_size" type="text"
                                   value="<c:out value="${entry.key}"/>"
                                   name="optionNameRus${counter.count}" required/>
                        </div>
                        <div class="float_child">
                            <input class="max_size" type="text"
                                   value="<c:out value="${entry.value}"/>"
                                   name="optionValueRus${counter.count}" required/>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <div class="float_child">
                <fieldset>
                    <label><fmt:message key="label.product"/></label><br/>
                    <input class="max_size" placeholder="<fmt:message key="label.product"/>" name="nameEng" type="text"
                           value="<c:out value="${productEng.name}"/>" tabindex="1" required autofocus/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.brand"/></label>
                    <input class="max_size" placeholder="<fmt:message key="label.brand"/>" name="brandEng" type="text"
                           value="<c:out value="${productEng.brand}"/>" tabindex="2" required autofocus/>
                </fieldset>
                <br/>

                <fieldset>
                    <label><fmt:message key="label.descriptionProduct"/></label>
                    <textarea style="width: 100%; height: 400px;"
                              placeholder="<fmt:message key="label.descriptionProduct"/>" tabindex="4"
                              name="descriptionEng"
                              required><c:out value="${productEng.description}"/></textarea>
                </fieldset>
                <br/>


                <c:set var="optionsMap" value="${productEng.options.attributes}"/>

                <div class="float_container">
                    <c:forEach var="entry" items="${optionsMap}" varStatus="counter">
                        <div class="float_child">
                            <input class="max_size" type="text"
                                   value="<c:out value="${entry.key}"/>"
                                   name="optionNameEng${counter.count}" required/>
                        </div>
                        <div class="float_child">
                            <input class="max_size" type="text"
                                   value="<c:out value="${entry.value}"/>"
                                   name="optionValueEng${counter.count}" required/>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <button name="submit" type="submit" id="contact-submit"><fmt:message
                key="button.submit"/></button>
        <br/>

    </form>
</div>