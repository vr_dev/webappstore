<%@ include file="header.jsp" %>

<section class="slider_section ">
    <div class="carousel-item active">
        <div class="container ">
            <div class="row">
                <div class="col-md-6">
                    <div class="detail-box">
                        <h1>
                            <fmt:message key="label.home"/>
                        </h1>
                        <p>
                            <fmt:message key="label.home2"/>
                        </p>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="img-box">
                        <img src="images/slider-img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>


<!-- product section -->

<section class="product_section layout_padding">
    <c:if test="${sessionScope.user.isAdmin() == true}">
        <div class="heading_container heading_center">

            <div class="form-sh"><a role="button" class="btn btn-lg" href="createProduct"><fmt:message
                    key="button.createProduct"/></a></div>
            <div class="form-sh"><a class="btn btn-lg" href="allUsers"><fmt:message key="button.manageUsers"/></a></div>
            <div class="form-sh"><a class="btn btn-lg" href="ordersAdmin"><fmt:message key="button.orders"/></a></div>
        </div>
    </c:if>
    <c:if test="${sessionScope.user.isAdmin() == false}">
        <div class="heading_container heading_center">
            <div class="col-sm-3">
                <div class="form-sh"><a class="btn" href="ordersUser"><fmt:message key="button.myOrders"/></a></div>
            </div>
        </div>
    </c:if>
    <div class="container">
        <div class="heading_container heading_center">
            <h2>
                <fmt:message key="label.categories"/>
            </h2>
        </div>
        <div class="row">
            <c:forEach var="category" items="${categories }">
                <div class="col-sm-6 col-lg-4">
                    <a href="productCatalog?productCategory=${category.getId() }">
                        <div class="box">
                            <div class="detail-box">
                                <div class="img-box">
                                    <img src="${category.getThumbUrl()}" alt="">
                                </div>
                                <h5>
                                        ${category.getName()}
                                </h5>
                            </div>
                        </div>
                    </a>
                </div>
            </c:forEach>

        </div>

    </div>
</section>

<!-- end product section -->


<%@ include file="footer.jsp" %>
