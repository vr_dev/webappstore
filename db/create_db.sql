-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema store
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `store`;

-- -----------------------------------------------------
-- Schema store
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `store` DEFAULT CHARACTER SET utf8mb3;
-- -----------------------------------------------------
-- Schema store
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `store`;

-- -----------------------------------------------------
-- Schema store
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `store` DEFAULT CHARACTER SET utf8mb3;
USE `store`;

-- -----------------------------------------------------
-- Table `store`.`product_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product_category`;

CREATE TABLE IF NOT EXISTS `store`.`product_category`
(
    `id`        INT           NOT NULL AUTO_INCREMENT,
    `thumb_url` VARCHAR(2000) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 7
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `store`.`i18n`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`i18n`;

CREATE TABLE IF NOT EXISTS `store`.`i18n`
(
    `id`       INT         NOT NULL,
    `language` VARCHAR(45) NOT NULL,
    `code`     VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `language` (`language` ASC, `code` ASC) VISIBLE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `store`.`product_category_i18n`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product_category_i18n`;

CREATE TABLE IF NOT EXISTS `store`.`product_category_i18n`
(
    `id`            INT          NOT NULL AUTO_INCREMENT,
    `category_id`   INT          NOT NULL,
    `category_name` VARCHAR(255) NOT NULL,
    `lang_id`       INT          NOT NULL,
    PRIMARY KEY (`id`),
    INDEX (`category_id` ASC) VISIBLE,
    INDEX (`lang_id` ASC) VISIBLE,
    FOREIGN KEY (`category_id`)
        REFERENCES `store`.`product_category` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (`lang_id`)
        REFERENCES `store`.`i18n` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    UNIQUE (`category_id`, `lang_id`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product`;

CREATE TABLE IF NOT EXISTS `store`.`product`
(
    `id`          INT            NOT NULL AUTO_INCREMENT,
    `category_id` INT            NOT NULL,
    `create_time` TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `price`       DECIMAL(10, 2) NOT NULL,
    `image_url`   VARCHAR(2000)  NULL     DEFAULT NULL,
    `thumb_url`   VARCHAR(2000)  NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `category_id` (`category_id` ASC) VISIBLE,
    FOREIGN KEY (`category_id`)
        REFERENCES `store`.`product_category` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 181
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `store`.`options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`options`;

CREATE TABLE IF NOT EXISTS `store`.`options`
(
    `id`          INT NOT NULL AUTO_INCREMENT,
    `category_id` INT NOT NULL,
    PRIMARY KEY (`id`,`category_id`),
    INDEX `category_id` (`category_id` ASC) VISIBLE,
    FOREIGN KEY (`category_id`)
        REFERENCES `store`.`product_category` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 78
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `store`.`product_options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product_options`;

CREATE TABLE IF NOT EXISTS `store`.`product_options`
(
    `id`         INT NOT NULL AUTO_INCREMENT,
    `product_id` INT NOT NULL,
    `options_id` INT NOT NULL,
    PRIMARY KEY (`id`, `product_id`, options_id),
    INDEX `product_id` (`product_id` ASC) VISIBLE,
    INDEX `options_id` (`options_id` ASC) VISIBLE,
    FOREIGN KEY (`product_id`)
        REFERENCES `store`.`product` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (`options_id`)
        REFERENCES `store`.`options` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 5274
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `store`.`product_options_i18n`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product_options_i18n`;

CREATE TABLE IF NOT EXISTS `store`.`product_options_i18n`
(
    `id`                 INT          NOT NULL AUTO_INCREMENT,
    `product_options_id` INT          NOT NULL,
    `value`              VARCHAR(255) NOT NULL,
    `lang_id`            INT          NOT NULL,
    PRIMARY KEY (`id`),
    INDEX (`lang_id` ASC) VISIBLE,
    INDEX (`product_options_id` ASC) VISIBLE,
    FOREIGN KEY (`lang_id`)
        REFERENCES `store`.`i18n` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (`product_options_id`)
        REFERENCES `store`.`product_options` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    UNIQUE (`product_options_id`, `lang_id`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store`.`options_i18n`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`options_i18n`;

CREATE TABLE IF NOT EXISTS `store`.`options_i18n`
(
    `id`          INT          NOT NULL AUTO_INCREMENT,
    `options_id`  INT          NOT NULL,
    `option_name` VARCHAR(100) NOT NULL,
    `lang_id`     INT          NOT NULL,
    PRIMARY KEY (`id`),
    INDEX (`options_id` ASC) VISIBLE,
    INDEX (`lang_id` ASC) VISIBLE,
    FOREIGN KEY (`options_id`)
        REFERENCES `store`.`options` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (`lang_id`)
        REFERENCES `store`.`i18n` (`id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    UNIQUE (`options_id`,`option_name`, `lang_id`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store`.`product_i18n`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product_i18n`;

CREATE TABLE IF NOT EXISTS `store`.`product_i18n`
(
    `id`          INT          NOT NULL AUTO_INCREMENT,
    `product_id`  INT          NOT NULL,
    `name`        VARCHAR(255) NOT NULL,
    `brand`       VARCHAR(255) NOT NULL,
    `description` TEXT         NULL,
    `lang_id`     INT          NOT NULL,
    PRIMARY KEY (`id`),
    INDEX (`product_id` ASC) VISIBLE,
    INDEX (`lang_id` ASC) VISIBLE,
    FOREIGN KEY (`product_id`)
        REFERENCES `store`.`product` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (`lang_id`)
        REFERENCES `store`.`i18n` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    UNIQUE (`product_id`, `lang_id`)
)
    ENGINE = InnoDB;

USE `store`;

-- -----------------------------------------------------
-- Table `store`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`role`;

CREATE TABLE IF NOT EXISTS `store`.`role`
(
    `id`   INT         NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(20) NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 3
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `store`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`user`;

CREATE TABLE IF NOT EXISTS `store`.`user`
(
    `id`         INT          NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(45)  NOT NULL,
    `last_name`  VARCHAR(45)  NOT NULL,
    `email`      VARCHAR(320) NOT NULL,
    `password`   VARCHAR(160) NOT NULL,
    `role_id`    INT          NOT NULL,
    `blocked`    BIT(1)       NOT NULL DEFAULT b'0',
    PRIMARY KEY (`id`, `role_id`),
    UNIQUE INDEX `email` (`email` ASC) VISIBLE,
    INDEX `role_id` (`role_id` ASC) VISIBLE,
    FOREIGN KEY (`role_id`)
        REFERENCES `store`.`role` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `store`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`status`;

CREATE TABLE IF NOT EXISTS `store`.`status`
(
    `id`   INT          NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `status_id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 4
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `store`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`order`;

CREATE TABLE IF NOT EXISTS `store`.`order`
(
    `id`          INT       NOT NULL AUTO_INCREMENT,
    `user_id`     INT       NOT NULL,
    `status_id`   INT       NOT NULL,
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`, `status_id`),
    INDEX `user_id` (`user_id` ASC) VISIBLE,
    INDEX `status_id` (`status_id` ASC) VISIBLE,
    CONSTRAINT `order_ibfk_1`
        FOREIGN KEY (`user_id`)
            REFERENCES `store`.`user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `order_ibfk_2`
        FOREIGN KEY (`status_id`)
            REFERENCES `store`.`status` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `store`.`order_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`order_content`;

CREATE TABLE IF NOT EXISTS `store`.`order_content`
(
    `order_id`   INT            NOT NULL,
    `product_id` INT            NOT NULL,
    `quantity`   INT            NOT NULL DEFAULT '1',
    `price`      DECIMAL(10, 2) NOT NULL,
    INDEX `order_id` (`order_id` ASC) VISIBLE,
    INDEX `product_id` (`product_id` ASC) VISIBLE,
    CONSTRAINT `order_content_ibfk_1`
        FOREIGN KEY (`order_id`)
            REFERENCES `store`.`order` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `order_content_ibfk_2`
        FOREIGN KEY (`product_id`)
            REFERENCES `store`.`product` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `store`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `store`;
INSERT INTO `store`.`role` (`id`, `name`)
VALUES (1, 'administrator');
INSERT INTO `store`.`role` (`id`, `name`)
VALUES (2, 'user');

COMMIT;


-- -----------------------------------------------------
-- Data for table `store`.`status`
-- -----------------------------------------------------
START TRANSACTION;
USE `store`;
INSERT INTO `store`.`status` (`id`, `name`)
VALUES (1, 'Pending');
INSERT INTO `store`.`status` (`id`, `name`)
VALUES (2, 'Paid');
INSERT INTO `store`.`status` (`id`, `name`)
VALUES (3, 'Cancelled');

COMMIT;

-- -----------------------------------------------------
-- Data for table `store`.`i18n`
-- -----------------------------------------------------
START TRANSACTION;
USE `store`;
INSERT INTO `store`.`i18n` (`id`, `language`, `code`)
VALUES (1, 'Русский', 'ru');
INSERT INTO `store`.`i18n` (`id`, `language`, `code`)
VALUES (2, 'English', 'en');

COMMIT;